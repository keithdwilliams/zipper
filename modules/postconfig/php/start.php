﻿<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:	
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Start Pre-Config');
endif;
?>
<link rel="stylesheet" href="/SSoD-CSS0007" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0151"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" name="page">
			<div id="header" name="header">
				<a name="home" id="home" href="javascript:void(0);" tabindex="9">Home</a>
				<a name="logout" id="logout" href="javascript:void(0);" tabindex="10">Logout</a>
				<h2>ZIPPER</h2>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>	
			<div id="contentbox" name="contentbox">
				<h3>Post-Configure Information</h3>
					<form>
						<div id="dyncontent" name="dyncontent">
							<div>
								<h4>Host / Machine</h4>
								<?php
								if(isset($_SESSION['zipper']['hostmachine'] -> machine) && $_SESSION['zipper']['hostmachine'] -> machine != '') :
									echo '<input id="host" name="host" class="hostid config" placeholder="Fully qualified host name" type="text" style="width:405px;margin-bottom:2px;" tabindex="1" autofocus="autofocus" require="required" value="'.$_SESSION['zipper']['hostmachine'] -> machine.'" autocomplete="off" />';
								else:
									echo '<input id="host" name="host" class="hostid config" placeholder="Fully qualified host name" type="text" style="width:405px;margin-bottom:2px;" tabindex="1" autofocus="autofocus" require="required" autocomplete="off" />';
								endif;
								?>
							</div>
							<div id="results" name="results">
							</div>
							<hr />
							<div style="width:420px;margin:0px auto;">
								<h4>Servers Scripts to be Configured</h4>
								<div class="pstconfig"><input type="checkbox" id="sasserver" name="sasserver" /><span>SAS Servers</span></div>
								<div class="pstconfig"><input type="checkbox" id="midtier" name="midtier" /><span>Middle Tier</span></div>
								<div class="pstconfig"><input type="checkbox" id="fedsrv" name="fedsrv" /><span>Federation Server</span></div>
								<div class="pstconfig"><input type="checkbox" id="fedsrvmgr" name="fedsrvmgr" /><span>Federation Server</span></div>
								<div class="pstconfig"><input type="checkbox" id="dfmgr" name="dfmgr" /><span>Dataflux Manager</span></div>
								<div class="pstconfig"><input type="checkbox" id="sdm" name="sdm" /><span>SDM</span></div>
								<div class="pstconfig"><input type="checkbox" id="dfauth" name="dfauth" /><span>Dataflux Authentication Server</span></div>
								<div class="pstconfig"><input type="checkbox" id="lsf" name="pm" /><span>LSF & Platform Process Manager</span></div>
							</div>
							<hr />
								<h4>Host Specific Information</h4>
								<div class="pstconfig"><input type="checkbox" id="qkb" name="qkb" /><span>Dataflux QKB</span></div>
								<div class="pstconfig"><input type="checkbox" id="jbsrv" name="jbsrv" /><span>Middle Tier</span></div>
								<div class="pstconfig"><input type="checkbox" id="sasaccess" name="sasaccess" /><span>SAS/Access</span></div>
								<div class="pstconfig"><input type="checkbox" id="foundation" name="foundation" /><span>SAS Foundation</span></div>
								<div id="databases" name="databases" class="pstconfig">
									<input type="radio" id="accessdb" name="accessdb" value="oracle" />Oracle
									<input type="radio" id="accessdb" name="accessdb" value="mysql" />MySql
								</div>
							<hr />
							<h4>Additional Environment Information</h4>

							<div style="float:left;">
								<?php
								if(isset($_SESSION['zipper']['hostmachine'] -> ownerid) && $_SESSION['zipper']['hostmachine'] -> ownerid != '') :
									echo '<input id="ownerid" name="ownerid" class="name config" placeholder="CID Owner ID" type="text" tabindex="6" value="'.$_SESSION['zipper']['hostmachine'] -> ownerid.'" require="required" />';
								else:
									echo '<input id="ownerid" name="ownerid" class="name config" placeholder="CID Owner ID" type="text" tabindex="6" value="" require="required" autocomplete="off" />';
								endif;
								?>
							</div>
							<div style="float:right;">
								<?php
								if(isset($_SESSION['zipper']['hostmachine'] -> ownerpwd) && $_SESSION['zipper']['hostmachine'] -> ownerpwd != '') :
									echo '<input id="ownerpwd" name="ownerpwd" class="password config" placeholder="CID Owner Password" type="password" tabindex="7" value="'.$_SESSION['zipper']['hostmachine'] -> ownerpwd.'" require="required" autocomplete="off" />';
								else:
									echo '<input id="ownerpwd" name="ownerpwd" class="password config" placeholder="CID Owner Password" type="password" tabindex="7" require="required" autocomplete="off" />';
								endif;
								?>
							</div>
							<div style="clear:both;"></div>
							<?php					
							$mysqli = @new mysqli(HOSTi, DBUSERi, PASSi, DBi);
							if(mysqli_connect_errno()):
								echo '
								<input id="depotdir" type="hidden" value = "" />
								<input id="configdir" type="hidden" value = "" />
								<input id="sashomedir" type="hidden" value = "" />
								<input id="jbossdir" type="hidden" value = "" />
								<input id="jdkdir" type="hidden" value = "" />
								<input id="admindir" type="hidden" value = "" />
								<input id="lsfdir" type="hidden" value = "" />
								<input id="pmdir" type="hidden" value = "" />';
							else:
								if($stmt = $mysqli -> prepare("SELECT depot, config, sashome, jboss, jdk, admin, lsf, pm FROM zipper_environment WHERE username = ? LIMIT 1")):
									$stmt -> bind_param('s', $_SESSION['zipper']['zipper'] -> username);
									$stmt -> execute();
									$stmt -> store_result();
									if($stmt -> num_rows != 0):
										$stmt -> bind_result($depot, $config, $sashome, $jboss, $jdk, $admin, $lsf, $pm);
										$stmt -> fetch();
										echo '
										<input id="depotdir" type="hidden" value = "'.$depot.'" />
										<input id="configdir" type="hidden" value = "'.$config.'" />
										<input id="sashomedir" type="hidden" value = "'.$sashome.'" />
										<input id="jbossdir" type="hidden" value = "'.$jboss.'" />
										<input id="jdkdir" type="hidden" value = "'.$jdk.'" />
										<input id="admindir" type="hidden" value = "'.$admin.'" />
										<input id="lsfdir" type="hidden" value = "'.$lsf.'" />
										<input id="pmdir" type="hidden" value = "'.$pm.'" />';
									else:
										echo '
										<input id="depotdir" type="hidden" value = "" />
										<input id="configdir" type="hidden" value = "" />
										<input id="sashomedir" type="hidden" value = "" />
										<input id="jbossdir" type="hidden" value = "" />
										<input id="jdkdir" type="hidden" value = "" />
										<input id="admindir" type="hidden" value = "" />
										<input id="lsfdir" type="hidden" value = "" />
										<input id="pmdir" type="hidden" value = "" />';
									endif;
									$stmt -> close();
									$mysqli -> close();
								else:
									echo '
									<input id="depotdir" type="hidden" value = "" />
									<input id="configdir" type="hidden" value = "" />
									<input id="sashomedir" type="hidden" value = "" />
									<input id="jbossdir" type="hidden" value = "" />
									<input id="jdkdir" type="hidden" value = "" />
									<input id="admindir" type="hidden" value = "" />
									<input id="lsfdir" type="hidden" value = "" />
									<input id="pmdir" type="hidden" value = "" />';
								endif;
							endif;
							?>
						</div>
						<div style="text-align:center;width:100%;">
							<button id="start" name="start" class="action bluebtn" tabindex="10" /><span class="label">Start</span></button>
							<button id="reset" name="reset" class="action redbtn" tabindex="10" /><span class="label">Reset</span></button>
							<button id="verify" name="verify" class="action greenbtn" tabindex="10" /><span class="label">Verify Environment</span></button>
							<button id="clear" name="clear" class="action redbtn" tabindex="10" /><span class="label">Clear</span></button>
						</div>
					</form>
			</div>

		</div>
		<div id="footer">
		    <span>(C) Zipper</span>
		</div>

	</div>
</div>
</body>
</html>
