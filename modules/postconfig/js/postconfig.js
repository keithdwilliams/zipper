﻿$(document).ready(function () {

	$('input#midtier').change(function() {
		if($('input#midtier').is(':checked')) {
			$('input#jbsrv').attr('checked', true);
		} else {
			$('input#jbsrv').attr('checked', false);		
		}
	});

	$('input#jbsrv').change(function() {
		if($('input#jbsrv').is(':checked')) {
			$('input#midtier').attr('checked', true);
		} else {
			$('input#midtier').attr('checked', false);		
		}
	});

	$('input#sasaccess').change(function() {
		if($('input#sasaccess').is(':checked')) {
			$('div#databases').css('display', 'block');
		} else {
			$('div#databases').css('display', 'none');		
		}
	});
	
	if(localStorage.getItem("hostname") !== null) {
		$('input#host').val(localStorage.hostname);
	}
	
	$('input#host').blur(function() {
		if($('input#host').val() != '') {
			localStorage.hostname = $('input#host').val();
		}
	});


	/*///////////////////////
	/////Precheck Script/////
	///////////////////////*/
	
	$('button#verify').bind('click', function(e){
		
		/*////////////////////////////////////////////
		/////lets validate data on the login form/////
		////////////////////////////////////////////*/
		
		var reason = '';
		reason += validateHost(this.form.host);
		reason += validateName(this.form.ownerid);
		reason += validatePassword(this.form.ownerpwd);
		
		/*//////////////////////////////////////////////////
		/////if there is a reason the form is not valid///// 
		/////then we will send a popup with the reason /////
		//////////////////////////////////////////////////*/
		
		if (reason != "") {
			var x = document.getElementById('formAlert');
			x.innerHTML = reason;
			$('#linkAlert').click();
			
			/*////////////////////////////////////////////////
			/////The data in the form appears to be valid/////
			////////////////////////////////////////////////*/

			} else {
			
			/*////////////////////////////////////////////
			/////Check for valid environment settings/////
			////////////////////////////////////////////*/
			
			$('div#tmpnotice').remove();
			
			if($('input#midtier').is(':checked')) {
				if($('input#jbossdir').val() == '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">Unable to retrieve environment information.</sub></div>');
					exit;
				} else {
					$.ajax({
						type: 'POST',
						url: '/SSoD-0168',
						data: {
							host : host,
							jbossdir : jbossdir,
						},
						dataType: 'text',
						timeout: 10000,
						beforeSend: function() {
							$('div#pptxt').html('Please wait while the Configuration<br />Directory is validated');
							$('#prepage').show();
						},	
						success: function(text){

							/*//////////////////////////////////////////////////
							/////trim the whitespace from the text response/////
							//////////////////////////////////////////////////*/

							text = $.trim(text);

							/*////////////////////////////////////////////////////////////////
							/////Evaluate the text returned from the server ajax function/////
							////////////////////////////////////////////////////////////////*/

							switch(text) {
								case '01':
									myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated Configuration Directory<font></div>';
									$('div#results').append(myhtml);
									break;
								case '02':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Configuration Directory validation failed</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '03':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '04':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Unable to upload validation script</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '05':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Configuration Directory was not received</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '06':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">VSP Credentials will not validate on '+host+'</font></div>';
									$('div#results').append(myhtml);
									exit;
								default:
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
									$('div#results').append(myhtml);
									exit;
							}
						},
						complete: function(){
							//store the value in the session
							sessionStorage.jbossdir = $('input#jbossdir').val();
						}
					});
				}
			}

			if($('input#midtier').is(':checked') || $('input#fedsrv').is(':checked') || $('input#fedsrvmgr').is(':checked') || $('input#dfmgr').is(':checked') || $('input#sdm').is(':checked') || $('input#dfauth').is(':checked') || $('input#lsf').is(':checked')) {
				if($('input#admindir').val() == '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">Unable to retrieve environment information.</sub></div>');
					exit;
				} else {
					$.ajax({
						type: 'POST',
						url: '/SSoD-0167',
						data: {
							host : host,
							admindir : admindir,
						},
						dataType: 'text',
						timeout: 10000,
						beforeSend: function() {
							$('div#pptxt').html('Please wait while the Configuration<br />Directory is validated');
							$('#prepage').show();
						},	
						success: function(text){

							/*//////////////////////////////////////////////////
							/////trim the whitespace from the text response/////
							//////////////////////////////////////////////////*/

							text = $.trim(text);

							/*////////////////////////////////////////////////////////////////
							/////Evaluate the text returned from the server ajax function/////
							////////////////////////////////////////////////////////////////*/

							switch(text) {
								case '01':
									myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated Configuration Directory<font></div>';
									$('div#results').append(myhtml);
									break;
								case '02':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Configuration Directory validation failed</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '03':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '04':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Unable to upload validation script</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '05':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Configuration Directory was not received</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '06':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">VSP Credentials will not validate on '+host+'</font></div>';
									$('div#results').append(myhtml);
									exit;
								default:
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
									$('div#results').append(myhtml);
									exit;
							}
						},
						complete: function(){
							sessionStorage.admindir = $('input#admindir').val();
						}
					});
				}
			}
			
			if($('input#fedsrv').is(':checked') || $('input#dfauth').is(':checked') || $('input#foundation').is(':checked') || $('input#dfmgr').is(':checked')){
				if($('input#sashomedir').val() == '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">Unable to retrieve environment information.</sub></div>');
					exit;
				} else {
					$.ajax({
						type: 'POST',
						url: '/SSoD-0169',
						data: {
							host : host,
							sashomedir : sashomedir,
						},
						dataType: 'text',
						timeout: 10000,
						beforeSend: function() {
							$('div#pptxt').html('Please wait while the Configuration<br />Directory is validated');
							$('#prepage').show();
						},	
						success: function(text){

							/*//////////////////////////////////////////////////
							/////trim the whitespace from the text response/////
							//////////////////////////////////////////////////*/

							text = $.trim(text);

							/*////////////////////////////////////////////////////////////////
							/////Evaluate the text returned from the server ajax function/////
							////////////////////////////////////////////////////////////////*/

							switch(text) {
								case '01':
									myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated Configuration Directory<font></div>';
									$('div#results').append(myhtml);
									break;
								case '02':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Configuration Directory validation failed</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '03':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '04':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Unable to upload validation script</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '05':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Configuration Directory was not received</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '06':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">VSP Credentials will not validate on '+host+'</font></div>';
									$('div#results').append(myhtml);
									exit;
								default:
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
									$('div#results').append(myhtml);
									exit;
							}
						},
						complete: function(){
							sessionStorage.sashomedir = $('input#sashomedir').val();
						}
					});
				}
			}

			if($('input#sasserver').is(':checked')){			
				if($('input#configdir').val() == '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">Unable to retrieve environment information.</sub></div>');
					exit;
				} else {
					$.ajax({
						type: 'POST',
						url: '/SSoD-0166',
						data: {
							host : host,
							configdir : configdir,
						},
						dataType: 'text',
						timeout: 10000,
						beforeSend: function() {
							$('div#pptxt').html('Please wait while the Configuration<br />Directory is validated');
							$('#prepage').show();
						},	
						success: function(text){

							/*//////////////////////////////////////////////////
							/////trim the whitespace from the text response/////
							//////////////////////////////////////////////////*/

							text = $.trim(text);

							/*////////////////////////////////////////////////////////////////
							/////Evaluate the text returned from the server ajax function/////
							////////////////////////////////////////////////////////////////*/

							switch(text) {
								case '01':
									myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated Configuration Directory<font></div>';
									$('div#results').append(myhtml);
									break;
								case '02':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Configuration Directory validation failed</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '03':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '04':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Unable to upload validation script</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '05':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Configuration Directory was not received</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '06':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">VSP Credentials will not validate on '+host+'</font></div>';
									$('div#results').append(myhtml);
									exit;
								default:
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
									$('div#results').append(myhtml);
									exit;
							}
						},
						complete: function(){
							sessionStorage.configdir = $('input#configdir').val();
						}
					});
				}
			}

			if($('input#midtier').is(':checked')){
				if($('input#jdkdir').val() == '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">Unable to retrieve environment information.</sub></div>');
					exit;
				} else {
					$.ajax({
						type: 'POST',
						url: '/SSoD-0170',
						data: {
							host : host,
							jdkdir : jdkdir,
						},
						dataType: 'text',
						timeout: 10000,
						beforeSend: function() {
							$('div#pptxt').html('Please wait while the Configuration<br />Directory is validated');
							$('#prepage').show();
						},	
						success: function(text){

							/*//////////////////////////////////////////////////
							/////trim the whitespace from the text response/////
							//////////////////////////////////////////////////*/

							text = $.trim(text);

							/*////////////////////////////////////////////////////////////////
							/////Evaluate the text returned from the server ajax function/////
							////////////////////////////////////////////////////////////////*/

							switch(text) {
								case '01':
									myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated Configuration Directory<font></div>';
									$('div#results').append(myhtml);
									break;
								case '02':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Configuration Directory validation failed</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '03':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '04':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Unable to upload validation script</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '05':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Configuration Directory was not received</font></div>';
									$('div#results').append(myhtml);
									exit;
								case '06':
									/*update content with failure*/
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">VSP Credentials will not validate on '+host+'</font></div>';
									$('div#results').append(myhtml);
									exit;
								default:
									myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
									$('div#results').append(myhtml);
									exit;
							}
						},
						complete: function(){
							sessionStorage.jdkdir = $('input#jdkdir').val();
						}
					});
				}
			}
			
			var host = $('input#host').val();
			var ownerid = $('input#ownerid').val();
			var ownerpwd = $('input#ownerpwd').val();
			$('div#results').html('');
			$('button#verify').css('display', 'none');
			$('button#clear').css('display', 'none');
			$('button#start').css('display', 'inline-block');
			$('button#reset').css('display', 'inline-block');
			$('div#results').append('<h5 style="width:100%;text-align:center;"><font color="red">If you are satisfied with the results<br />then select the start button below.</font></h5>');
			
			/*/////////////////////
			/////Validate Host/////
			/////////////////////*/
			
			$.ajax({
				type: 'POST',
				url: '/SSoD-0151',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while '+host+'<br />is validated');
					$('#prepage').show();
				},	
				error: function (xhr, textStatus, errorThrown) {
					console.log(xhr.responseText);
				},
				success: function(text){
					
					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/
					
					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/
					
					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated '+host+'<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Host Validation failed</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Host Validation failed</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					sasserver(host);
				}
			});
		}
		
		/*///////////////////////////////////////////
		/////stop the default action of the form/////
		///////////////////////////////////////////*/
		
		e.preventDefault();
	});
	
	function sasserver(host) {
	
		/*////////////////////////////////////////////////
		/////Validate if SAS Server can be configured/////
		////////////////////////////////////////////////*/

		if($('input#sasserver').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0152',
				data: {
					host : host,
					configdir : configdir,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while SAS Servers<br />are validated');
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated SAS Server<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Server validation failed</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Connection to '+host+' failed</font></div>';
							$('div#results').append(myhtml);
							break;
						case '04':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Unable to upload validation script</font></div>';
							$('div#results').append(myhtml);
							break;
						case '05':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Configuration Directory was not received</font></div>';
							$('div#results').append(myhtml);
							break;
						case '06':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">VSP Credentials will not validate on '+host+'</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					midtier(host);
				}
			});
		} else {
			midtier(host);
		}
	}	
	
	function midtier(host) {
			
		/*////////////////////////////////////////////////////
		/////Validate if the Middle Tier can be configure/////
		////////////////////////////////////////////////////*/

		if($('input#midtier').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0153',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while the Middle Tier<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated midtier<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Midtier Validation Failed</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Midtier Validation Failed</font></div>';
							$('div#results').append(myhtml);
							break;
						case '04':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Unable to upload validation script</font></div>';
							$('div#results').append(myhtml);
							break;
						case '05':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Configuration Directory was not received</font></div>';
							$('div#results').append(myhtml);
							break;
						case '06':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">VSP Credentials will not validate on '+host+'</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					lsf(host);
				}
			});
		} else {
			lsf(host);
		}
	}
	
	function lsf(host) {
			
		/*/////////////////////////////////////////
		/////Validate if lsf can be configured/////
		/////////////////////////////////////////*/

		if($('input#lsf').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0154',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while LSF<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated LSF<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">LSF Validation failed</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">LSF Validation failed</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					pm(host);
				}
			});
		} else {
			pm(host);
		}
	}
	
	function pm(host) {
			
		/*//////////////////////////////////////////////////////
		/////Validate if Platform Manager can be configured/////
		//////////////////////////////////////////////////////*/

		if($('input#pm').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0155',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while Platform Process Manager<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated Process Manager<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Process Manager Validation Failed</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Process Manager Validation Failed</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					fedsrv(host);
				}
			});
		} else {
			fedsrv(host);
		}
	}
	
	function fedsrv(host) {
			
		/*////////////////////////////////////////////////////////////////////
		/////Validate if the Dataflux Federation Server can be configured/////
		////////////////////////////////////////////////////////////////////*/

		if($('input#fedsrv').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0156',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while Dataflux Federation Server<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated Fedration Server<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Federation Server Validation failed</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Federation Server Validation failed.</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					fedsrvmgr(host);
				}
			});
		} else {
			fedsrvmgr(host);
		}
	}
	
	function fedsrvmgr(host) {
			
		/*////////////////////////////////////////////////////////////////////
		/////Validate if the Dataflux Federation Server can be configured/////
		////////////////////////////////////////////////////////////////////*/

		if($('input#fedsrvmgr').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0164',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while Dataflux Federation Server Manager<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated Fedration Server Manager<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Federation Server Manager Validation failed</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Federation Manager Server Validation failed.</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					dfmgr(host);
				}
			});
		} else {
			dfmgr(host);
		}
	}
	
	function dfmgr(host) {
			
		/*/////////////////////////////////
		/////Validate Dataflux Manager/////
		/////////////////////////////////*/

		if ($('input#dfmgr').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0158',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while Dataflux Manager<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated Dataflux Manager<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Dataflux Manager Failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Dataflux Manager Failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					sdm(host);
				}
			});
		} else {
			sdm(host);
		}
	}
	
	function sdm(host) {
			
		/*////////////////////////////////
		/////Validate Metadata Server/////
		////////////////////////////////*/			

		if ($('input#sdm').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0165',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while SAS Digital Marketing Server<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated SAS Digital Marketing Server<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Digital Marketing Server failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Digital Marketing Server failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					dfauth(host);
				}
			});
		} else {
			dfauth(host);
		}
	}
	
	function dfauth(host) {
			
		/*/////////////////////////////////////
		/////Validate Dataflux Auth Server/////
		/////////////////////////////////////*/

		if ($('input#dfauth').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0157',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while Dataflux Auth Server<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated Dataflux Auth Server<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Dataflux Auth Server validation failed</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Dataflux Auth Server validation failed</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					qkb(host);
				}
			});
		} else {
			qkb(host);
		}
	}
	
		
	function qkb(host) {

		/*////////////////////
		/////Validate QKB/////
		////////////////////*/

		if ($('input#qkb').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0160',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while Dataflux QKB<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated Dataflux QKB<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Dataflux QKB failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">Dataflux QKB failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					jbosssrv(host);
				}
			});
		} else {
			jbosssrv(host);
		}
	}
	
	function jbosssrv(host) {
			
		/*//////////////////////
		/////Validate JBoss/////
		//////////////////////*/

		if ($('input#jbsrv').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0162',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while JBoss Server<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated JBoss Server<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">JBoss Server failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">JBoss Server failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					sasaccess(host);
				}
			});
		} else {
			sasaccess(host);
		}
	}
	
	function sasaccess(host) {
			
		/*///////////////////////////
		/////Validate SAS/Access/////
		///////////////////////////*/

		if ($('input#sasaccess').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0163',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while SAS/Access<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated SAS/Access<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS/Access failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS/Access failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
					foundation(host);
				}
			});
		} else {
			foundation(host);
		}
	}
	
	function foundation(host) {
			
		/*///////////////////////////
		/////Validate SAS/Access/////
		///////////////////////////*/

		if ($('input#foundation').is(':checked')) {
			$.ajax({
				type: 'POST',
				url: '/SSoD-0159',
				data: {
					host : host,
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {
					$('div#pptxt').html('Please wait while SAS Foundation<br />is validated');
					$('#prepage').show();
				},	
				success: function(text){

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display:inline;padding-right:20px;" /><font color="green">Successfully validated SAS Foundation<font></div>';
							$('div#results').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Foundation failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">SAS Foundation failed to validate</font></div>';
							$('div#results').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display:inline;padding-right:20px;" /><font color="red">'+text+'</font></div>';
							$('div#results').append(myhtml);
							break;
					}
				},
				complete: function(){
					$('#prepage').hide();
				}
			});
		}
	}
	
	$('button#start').bind('click', function(e){
	});
	
	$('button#reset').bind('click', function(e){
		window.top.location.href = '/SSoD-0150';
		e.preventDefault();
		e.preventDefault();
	});
	
	$('button#clear').bind('click', function(e){
		$('input#host').val('');
		$('input#ownerid').val('');
		$('input#ownerpwd').val('');
		$('input#sasserver').attr('checked', false);
		$('input#midtier').attr('checked', false);
		$('input#lsf').attr('checked', false);
		$('input#pm').attr('checked', false);
		$('input#fedsrv').attr('checked', false);
		$('input#fedsrvmgr').attr('checked', false);
		$('input#dfmgr').attr('checked', false);
		$('input#sdm').attr('checked', false);
		$('input#dfauth').attr('checked', false);
		$('input#qkb').attr('checked', false);
		$('input#jbsrv').attr('checked', false);
		$('input#sasaccess').attr('checked', false);
		$('input#foundation').attr('checked', false);
		e.preventDefault();
	});
	
});			