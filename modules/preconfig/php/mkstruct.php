﻿<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | File System');
endif;
require($_SERVER['DOCUMENT_ROOT'].'/api/Net/SSH2.php');
$user = $_SESSION['zipper']['zipper'] -> username;
$pass = $_SESSION['zipper']['zipper'] -> password;
$cidrun = $_SESSION['zipper']['hostmachine'] -> runid;
$cidowner = $_SESSION['zipper']['hostmachine'] -> ownerid;
$cidpwd = $_SESSION['zipper']['hostmachine'] -> ownerpwd;
$cid = $_SESSION['zipper']['hostmachine'] -> projectid;
$sasv = $_SESSION['zipper']['hostmachine'] -> sasversion;
$cmd1 = 'cd /sso/admin/bin;echo "'.$pass.'" | sudo -S chmod 755 *;sudo ./mkstruct.sh -d -i '.$cidowner.' -g '.$cid.'app -r '.$cidrun.' -b /'.$cid.' -p default -v '.$sasv.' > /dev/null 2>&1;ls -l /'.$cid;
$cmd2 = 'cd /sso/admin/bin;echo "'.$pass.'" | sudo -S chmod 755 *;sudo ./mkstruct.sh -i '.$cidowner.' -g '.$cid.'app -r '.$cidrun.' -b /'.$cid.' -p default -v '.$sasv.' > /dev/null 2>&1;ls -l /'.$cid;
$ipaddr = $_SESSION['zipper']['hostmachine'] -> machine;
$ssh = new Net_SSH2($ipaddr);
if (!$ssh->login($user, $pass)) {
	exit('Login Failed to host '.$ipaddr.'<br />'.$_SESSION['zipper']['hostmachine'] -> ip);
} else {
	if($_SESSION['zipper']['hostmachine'] -> mode == 'dev') {
		$ssh->enablePTY();
		echo $ssh->exec($cmd1);
		$output = $ssh->read();
	} elseif($_SESSION['zipper']['hostmachine'] -> mode == 'prod') {
		$ssh->enablePTY();
		echo $ssh->exec($cmd2);
		$output = $ssh->read();
	} else {
		$output = "Failed to find installation mode (production (test) or development";
	}
}

?>
<link rel="stylesheet" href="/SSoD-CSS0003" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0101"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" style="display:none;max-width:1060px;height:100%;border:0px;padding:0px;margin:0 auto;position:relative;">
			<div id="header">
				<a name="home" id="home" href="javascript:void(0);" style="float:left;position:relative;top:-14px;" tabindex="9">Home</a>
				<a name="logout" id="logout" href="javascript:void(0);" style="float:right;position:relative;top:-14px;" tabindex="10">Logout</a>
				<h2 style="float:right;position:relative;top:-28px;padding-right:6px;">ZIPPER</h2>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>	
			<div style="max-width:800px;border-style:solid;padding:4px;border-width:1px;border-color:blue;margin:0px auto;display:block;">
				<div style="overflow:auto;padding:10px;max-height:400px;">
					<h3>Creating the file system under /<?php echo  $cid ?> on <?php echo $_SESSION['zipper']['hostmachine'] -> machine; ?></h3>
					<?php
					$output = str_replace('[sudo] password for '.$user.': ', '', $output);
					echo '<pre>'.$output.'</pre>';
					?>
				</div>
			</div>
			<div style="width:475px;margin:0px auto;">
				<p>After verifying that the file system has been created select <font color="blue">"Next"</font> to begin install junit.</p>
				<p>You may also select <font color="blue">"Again"</font> to create the file system again.</p>
				<button id="redo" class="action bluebtn" tabindex="8" /><span class="label">Again</span></button>
				<button id="junit" class="action bluebtn" tabindex="8" /><span class="label">Next</span></button>
			</div>
		</div>
		<div id="footer">
		    <span style="float:right;">(C) Zipper</span>
		</div>

	</div>
</div>
</body>
</html>
