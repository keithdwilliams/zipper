<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	$user = $_SESSION['zipper']['zipper'] -> username;
	$pass = $_SESSION['zipper']['zipper'] -> password;
	$cmd = 'echo "'.$pass.'" | sudo -S -l';
	$host = $_SESSION['zipper']['hostmachine'] -> machine;
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Verify Access');
endif;
?>
<link rel="stylesheet" href="/SSoD-CSS0003" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0101"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" name="page">
			<div id="header" name="header">
				<a id="home" name="home" href="javascript:void(0);" tabindex="9">Home</a>
				<a id="logout" name="logout" href="javascript:void(0);" tabindex="10">Logout</a>
				<h2>ZIPPER</h2>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>
			<div id="cmdresults" name="cmdresults">
				<div>
					<h3>Verify Sudo Access</h3>
					<?php
					echo $_SESSION['zipper']['zipper'] -> runcmd($host, $user, $pass, $cmd);
					?>
				</div>
			</div>
			<div id="navcmd" name="navcmd">
				<p>Please verify that you have the correct access to continue.  Once done select <font color="blue">"Next"</font> to check the ulimit for this this installation.</p>
				<p>You may also select <font color="blue">"Again"</font> to check the sudo access once more</p>
				<button id="redo" class="action bluebtn" tabindex="8" /><span class="label">Again</span></button>
				<button id="ulimit" class="action bluebtn" tabindex="8" /><span class="label">Next</span></button>
			</div>
		</div>
		<div id="footer" name="footer">
		    <span>(C) Zipper</span>
		</div>
	</div>
</div>
</body>
</html>
