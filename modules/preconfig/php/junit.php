﻿<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | JUnit');
	require($_SERVER['DOCUMENT_ROOT'].'/api/Net/SSH2.php');
	$cidowner = $_SESSION['zipper']['hostmachine'] -> ownerid;
	$cidpwd = $_SESSION['zipper']['hostmachine'] -> ownerpwd;
	$cmd1 = 'cd /sso/sfw/junit;unzip -o junit4.8.1.zip';
	$ipaddr = $_SESSION['zipper']['hostmachine'] -> machine;
	$connection = ssh2_connect($ipaddr, 22);
	ssh2_auth_password($connection, $cidowner, $cidpwd);
	ssh2_scp_send($connection, '/var/www/html/software/junit/junit4.8.1.zip', '/sso/sfw/junit/junit4.8.1.zip', 0644);
endif;
?>
<link rel="stylesheet" href="/SSoD-CSS0003" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0101"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" style="display:none;max-width:1060px;height:100%;border:0px;padding:0px;margin:0 auto;position:relative;">
			<div id="header">
				<a name="home" id="home" href="javascript:void(0);" style="float:left;position:relative;top:-14px;" tabindex="9">Home</a>
				<a name="logout" id="logout" href="javascript:void(0);" style="float:right;position:relative;top:-14px;" tabindex="10">Logout</a>
				<h2 style="float:right;position:relative;top:-28px;padding-right:6px;">ZIPPER</h2>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>	
			<div style="max-width:800px;border-style:solid;padding:4px;border-width:1px;border-color:blue;margin:0px auto;display:block;">
				<div style="overflow:auto;padding:10px;max-height:400px;">
					<h3>Installing JUnit to <?php echo $_SESSION['zipper']['hostmachine'] -> machine; ?></h3>
					<?php
					$ssh = new Net_SSH2($ipaddr);
					if (!$ssh->login($cidowner, $cidpwd)) {
						exit('Login Failed to host '.$ipaddr.'<br />'.$_SESSION['zipper']['hostmachine'] -> ip);
					} else {
						echo '<pre>'.$ssh->exec($cmd1).'</pre><br />';
					}
					?>
				</div>
			</div>
			<div style="width:475px;margin:0px auto;text-align:center;">
				<p style="text-align:left;">After verifying that junit has been installed select <font color="blue">"Next"</font> to install SSO_RUN.</p>
				<p style="text-align:left;">You may also select <font color="blue">"Again"</font> to install JUnit again.</p>
					<button id="redo" class="action bluebtn" tabindex="1" /><span class="label">Again</span></button>
					<button id="ssorun" class="action bluebtn" tabindex="2" /><span class="label">Next</span></button>
			</div>
		</div>
		<div id="footer">
		    <span style="float:right;">(C) Zipper</span>
		</div>

	</div>
</div>
</body>
</html>
