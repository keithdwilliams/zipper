﻿<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:	
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Start Pre-Config');
endif;
?>
<link rel="stylesheet" href="/SSoD-CSS0003" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0101"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" name="page">
			<div id="header" name="header">
				<a name="home" id="home" href="javascript:void(0);" tabindex="9">Home</a>
				<a name="logout" id="logout" href="javascript:void(0);" tabindex="10">Logout</a>
				<h2>ZIPPER</h2>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>	
			<div id="contentbox" name="contentbox">
				<h3>Pre-Configure Information</h3>
				<form>
					<div>
						<?php
						if(isset($_SESSION['zipper']['hostmachine'] -> machine) && $_SESSION['zipper']['hostmachine'] -> machine != '') :
							echo '<input id="host" name="host" class="hostid config" placeholder="Fully qualified host name" type="text" style="width:405px;margin-bottom:2px;" tabindex="1" autofocus="autofocus" require="required" value="'.$_SESSION['zipper']['hostmachine'] -> machine.'" autocomplete="off" />';
						else:
							echo '<input id="host" name="host" class="hostid config" placeholder="Fully qualified host name" type="text" style="width:405px;margin-bottom:2px;" tabindex="1" autofocus="autofocus" require="required" autocomplete="off" />';
						endif;
						?>
					</div>
					
					<div style="float:left;">
						<?php
						if(isset($_SESSION['zipper']['hostmachine'] -> projectid) && $_SESSION['zipper']['hostmachine'] -> projectid != '') :
							echo '<input id="projid" name="projid" class="project config" placeholder="Project ID" type="text" tabindex="2" require="required" value="'.$_SESSION['zipper']['hostmachine'] -> projectid.'" autocomplete="off" />';
						else:
							echo '<input id="projid" name="projid" class="project config" placeholder="Project ID" type="text" tabindex="2" require="required" autocomplete="off" />';
						endif;
						?>
					</div>
					<div style="float:right;">
						<?php
						if(isset($_SESSION['zipper']['hostmachine'] -> machine) && $_SESSION['zipper']['hostmachine'] -> machine != '') :
							if($_SESSION['zipper']['hostmachine'] -> sasversion == '920'):
								echo '<select id="sasv" name="sasv" class="config" style="display:inline;height:35px;width:200px;" tabindex="3" require="required">';
									echo '<option value="">Select SAS Version</option>';
									echo '<option value="920" selected="selected">SAS 9.2</option>';
									echo '<option value="930">SAS 9.3</option>';
									echo '<option value="940">SAS 9.4</option>';
								echo '</select>';
							elseif($_SESSION['zipper']['hostmachine'] -> sasversion == '930'):
								echo '<select id="sasv" name="sasv" class="config" style="display:inline;height:35px;width:200px;" tabindex="3" require="required">';
									echo '<option value="">Select SAS Version</option>';
									echo '<option value="920">SAS 9.2</option>';
									echo '<option value="930" selected="selected">SAS 9.3</option>';
									echo '<option value="940">SAS 9.4</option>';
								echo '</select>';
							elseif($_SESSION['zipper']['hostmachine'] -> sasversion == '940'):
								echo '<select id="sasv" name="sasv" class="config" style="display:inline;height:35px;width:200px;" tabindex="3" require="required">';
									echo '<option value="">Select SAS Version</option>';
									echo '<option value="920">SAS 9.2</option>';
									echo '<option value="930">SAS 9.3</option>';
									echo '<option value="940" selected="selected">SAS 9.4</option>';
								echo '</select>';
							else:
								echo '<select id="sasv" name="sasv" class="config" style="display:inline;height:35px;width:200px;" tabindex="3" require="required">';
									echo '<option value="" selected="selected">Select SAS Version</option>';
									echo '<option value="920">SAS 9.2</option>';
									echo '<option value="930">SAS 9.3</option>';
									echo '<option value="940">SAS 9.4</option>';
								echo '</select>';
							endif;
						else:
							echo '<select id="sasv" name="sasv" class="config" style="display:inline;height:35px;width:200px;" tabindex="3" require="required">';
								echo '<option value="" selected="selected">Select SAS Version</option>';
								echo '<option value="920">SAS 9.2</option>';
								echo '<option value="930">SAS 9.3</option>';
								echo '<option value="940">SAS 9.4</option>';
							echo '</select>';
						endif;
						?>
					</div>
					<div style="clear:both;"></div>
					
					<div style="float:left;">
						<?php
						if(isset($_SESSION['zipper']['hostmachine'] -> location) && $_SESSION['zipper']['hostmachine'] -> location != '') :
							if($_SESSION['zipper']['hostmachine'] -> location == 'cary'):
								echo '<select id="location" name="location"  class="config"style="display:inline;height:35px;width:200px;" tabindex="4" require="required">
									<option value="">Select Hosting Location</option>
									<option value="cary" selected="selected">Cary</option>
									<option value="wynyard">Wynyard</option>
								</select>';
							elseif($_SESSION['zipper']['hostmachine'] -> location == 'wynyard'):
								echo '<select id="location" name="location"  class="config"style="display:inline;height:35px;width:200px;" tabindex="4" require="required">
									<option value="">Select Hosting Location</option>
									<option value="cary">Cary</option>
									<option value="wynyard" selected="selected">Wynyard</option>
								</select>';
							else:
								echo '<select id="location" name="location"  class="config"style="display:inline;height:35px;width:200px;" tabindex="4" require="required">
									<option value="" selected="selected">Select Hosting Location</option>
									<option value="cary">Cary</option>
									<option value="wynyard">Wynyard</option>
								</select>';
							endif;
						else:
							echo '<select id="location" name="location"  class="config"style="display:inline;height:35px;width:200px;" tabindex="4" require="required">
								<option value="" selected="selected">Select Hosting Location</option>
								<option value="cary">Cary</option>
								<option value="wynyard">Wynyard</option>
							</select>';
						endif;
						?>
					</div>
					<div style="float:right;">
						<?php
						if(isset($_SESSION['zipper']['hostmachine'] -> mode) && $_SESSION['zipper']['hostmachine'] -> mode != '') :
							if($_SESSION['zipper']['hostmachine'] -> mode == 'dev'):
								echo'<select id="mode" name="mode" class="config" style="display:inline;height:35px;width:200px;" tabindex="5" require="required">
									<option value="">Select Installation Grade</option>
									<option value="dev" selected="selected">Development</option>
									<option value="prod">Test or Production</option>
								</select>';
							elseif($_SESSION['zipper']['hostmachine'] -> mode == 'prod'):
								echo'<select id="mode" name="mode" class="config" style="display:inline;height:35px;width:200px;" tabindex="5" require="required">
									<option value="">Select Installation Grade</option>
									<option value="dev">Development</option>
									<option value="prod" selected="selected">Test or Production</option>
								</select>';
							else:
								echo'<select id="mode" name="mode" class="config" style="display:inline;height:35px;width:200px;" tabindex="5" require="required">
									<option value="" selected="selected">Select Installation Grade</option>
									<option value="dev">Development</option>
									<option value="prod">Test or Production</option>
								</select>';
							endif;
						else:
							echo'<select id="mode" name="mode" class="config" style="display:inline;height:35px;width:200px;" tabindex="5" require="required">
								<option value="" selected="selected">Select Installation Grade</option>
								<option value="dev">Development</option>
								<option value="prod">Test or Production</option>
							</select>';
						endif;
						?>
					</div>
					<div style="clear:both;"></div>
					
					<div style="float:left;">
						<?php
						if(isset($_SESSION['zipper']['hostmachine'] -> ownerid) && $_SESSION['zipper']['hostmachine'] -> ownerid != '') :
							echo '<input id="ownerid" name="ownerid" class="name config" placeholder="CID Owner ID" type="text" tabindex="6" value="'.$_SESSION['zipper']['hostmachine'] -> ownerid.'" require="required" />';
						else:
							echo '<input id="ownerid" name="ownerid" class="name config" placeholder="CID Owner ID" type="text" tabindex="6" value="" require="required" autocomplete="off" />';
						endif;
						?>
					</div>
					<div style="float:right;">
						<?php
						if(isset($_SESSION['zipper']['hostmachine'] -> ownerpwd) && $_SESSION['zipper']['hostmachine'] -> ownerpwd != '') :
							echo '<input id="ownerpwd" name="ownerpwd" class="password config" placeholder="CID Owner Password" type="password" tabindex="7" value="'.$_SESSION['zipper']['hostmachine'] -> ownerpwd.'" require="required" autocomplete="off" />';
						else:
							echo '<input id="ownerpwd" name="ownerpwd" class="password config" placeholder="CID Owner Password" type="password" tabindex="7" require="required" autocomplete="off" />';
						endif;
						?>
					</div>
					<div style="clear:both;"></div>
					
					<div style="float:left;">
						<?php
						if(isset($_SESSION['zipper']['hostmachine'] -> runid) && $_SESSION['zipper']['hostmachine'] -> runid != '') :
							echo '<input id="runid" name="runid" class="name config" placeholder="CID Run ID" type="text"" tabindex="8" value="'.$_SESSION['zipper']['hostmachine'] -> runid.'" require="required" autocomplete="off" />';
						else:
							echo '<input id="runid" name="runid" class="name config" placeholder="CID Run ID" type="text"" tabindex="8" require="required" autocomplete="off" />';
						endif;
						?>
					</div>
					<div style="clear:both;"></div>

					<div>
						<button id="start" name="start" class="action bluebtn" tabindex="10" /><span class="label">Start</span></button>
					</div>
				</form>
			</div>

		</div>
		<div id="footer">
		    <span>(C) Zipper</span>
		</div>

	</div>
</div>
</body>
</html>
