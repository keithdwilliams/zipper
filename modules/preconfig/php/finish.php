﻿<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Finish Pre-Config');
endif;
?>
<link rel="stylesheet" href="/SSoD-CSS0002" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0101"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" style="display:none;max-width:1060px;height:100%;border:0px;padding:0px;margin:0 auto;position:relative;">
			<div id="header">
				<h2 style="float:right;position:relative;top:-28px;">ZIPPER</h2>
				<a name="logout" id="logout" href="javascript:void(0);">Logout</a>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>	
			<div style="width:475px;border-style:solid;padding:4px;border-width:1px;border-color:blue;margin:0px auto;">
				<h3>Next Steps</h3>
				<ul>
					<li>
						<a href="javascript:void(0);" name="gomid" id="gomid" style="text-decoration:none;">Configure Middle Tier</a>
					</li>
					<li>
						<a href="javascript:void(0);" name="goagain" id="goagain" style="text-decoration:none;">Configure another machine</a>
					</li>
					<li>
						<a href="javascript:void(0);" name="loadesd" id="loadesd" style="text-decoration:none;">Load ESD Client</a>
					</li>
					<li>
						<a href="javascript:void(0);" name="gohome" id="gohome" style="text-decoration:none;">Go to home</a>
					</li>
				</ul>
			</div>
			<div style="clear:both;"></div>
		</div>
		<div id="footer">
		    (C) Footer
		</div>
	</div>
</div>
</body>
</html>