<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Ulimit');
	$user = $_SESSION['zipper']['zipper'] -> username;
	$pass = $_SESSION['zipper']['zipper'] -> password;
	if($_SESSION['zipper']['hostmachine'] -> sasversion == '940'):
		$cmd = 'ulimit -Hn;ulimit -Hu';
	else:
		$cmd = 'ulimit -Hn';
	endif;
	$host = $_SESSION['zipper']['hostmachine'] -> machine;
endif;
?>
<link rel="stylesheet" href="/SSoD-CSS0003" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0101"></script>
</head>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" name="page">
			<div id="header" name="header">
				<a name="home" id="home" href="javascript:void(0);" tabindex="10">Home</a>
				<a name="logout" id="logout" href="javascript:void(0);" tabindex="10">Logout</a>
				<h2>ZIPPER</h2>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>
			<div id="cmdresults" name="cmdresults">
				<div>
					<h3>Verify Process Resource Limit</h3>
					<?php
					echo $_SESSION['zipper']['zipper'] -> runcmd($host, $user, $pass, $cmd)
					?>
				</div>
			</div>
			<div id="navcmd" name="navcmd">
				<p>Please verify that the ulimit is correct to continue.  Once done select <font color="blue">"Next"</font> to begin the process of exporting the SAS Admin SVN repository.</p>
				<p>You may also select <font color="blue">"Again"</font> to check the ulimit access again.</p>
				<button id="redo" class="action bluebtn" tabindex="8" /><span class="label">Again</span></button>
				<button id="setsso" class="action bluebtn" tabindex="8" /><span class="label">Next</span></button>
			</div>
		</div>
		<div id="footer" name="footer">
		    <span">(C) Zipper</span>
		</div>

	</div>
</div>
</body>
</html>