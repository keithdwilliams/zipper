﻿<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | SSO Directory Security');
endif;
require($_SERVER['DOCUMENT_ROOT'].'/api/Net/SSH2.php');
$user = $_SESSION['zipper']['zipper'] -> username;
$pass = $_SESSION['zipper']['zipper'] -> password;
$cidowner = $_SESSION['zipper']['hostmachine'] -> ownerid;
$cmd = 'echo "'.$pass.'" | sudo -S chown -R '.$cidowner.':sasapp /sso > /dev/null 2>&1;sudo chmod 755 /sso > /dev/null 2>&1;ls -ld /sso';
$ipaddr = $_SESSION['zipper']['hostmachine'] -> machine;
$ssh = new Net_SSH2($ipaddr);
if (!$ssh->login($user, $pass)) {
	exit('Login Failed to host '.$ipaddr.'<br />'.$_SESSION['zipper']['hostmachine'] -> ip);
} else {
	$ssh->enablePTY();
	echo $ssh->exec($cmd);
	$output = $ssh->read();
}
?>
<link rel="stylesheet" href="/SSoD-CSS0003" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0101"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" style="display:none;max-width:1060px;height:100%;border:0px;padding:0px;margin:0 auto;position:relative;">
			<div id="header">
				<a name="home" id="home" href="javascript:void(0);" style="float:left;position:relative;top:-14px;" tabindex="9">Home</a>
				<a name="logout" id="logout" href="javascript:void(0);" style="float:right;position:relative;top:-14px;" tabindex="10">Logout</a>
				<h2 style="float:right;position:relative;top:-28px;padding-right:6px;">ZIPPER</h2>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>	
			<div style="max-width:800px;border-style:solid;padding:4px;border-width:1px;border-color:blue;margin:0px auto;display:block;">
				<div style="overflow:auto;padding:10px;max-height:400px;">
					<h3>Set SSO Directory Permission</h3>
					<?php
					$output = str_replace('[sudo] password for '.$user.': ', '', $output);
					echo '<pre>'.$output.'</pre>';
					?>
				</div>
			</div>
			<div style="width:475px;margin:0px auto;">
				<p>Please verify that you have the correct sso directory permissions to continue.  Once done select <font color="blue">"Next"</font> to begin the process of exporting the SAS Admin SVN repository.</p>
				<p>You may also select <font color="blue">"Again"</font> to set the directory permissions again.</p>
				<button id="redo" class="action bluebtn" tabindex="8" /><span class="label">Again</span></button>
				<button id="loadsvn" class="action bluebtn" tabindex="8" /><span class="label">Next</span></button>
			</div>
		</div>
		<div id="footer">
		    <span style="float:right;">(C) Zipper</span>
		</div>

	</div>
</div>
</body>
</html>
