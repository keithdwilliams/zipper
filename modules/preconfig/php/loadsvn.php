<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Load SVN');
	$user = $_SESSION['zipper']['zipper'] -> username;
	$pass = $_SESSION['zipper']['zipper'] -> password;
	$cidowner = $_SESSION['zipper']['hostmachine'] -> ownerid;
	$cidpwd = $_SESSION['zipper']['hostmachine'] -> ownerpwd;
	if($_SESSION['zipper']['hostmachine'] -> location == 'cary') {
		$cmd = 'cd /sso;svn --no-auth-cache --non-interactive --username '.$user.' --password '.$pass.' export https://svnhost.vsp.sas.com/sasadmins/trunk admin';
	} elseif($_SESSION['zipper']['hostmachine'] -> location == 'wynyard') {
		$cmd = 'cd /sso;svn --no-auth-cache --non-interactive --username '.$user.' --password '.$pass.' export https://svnhost-uk9.vsp.sas.com/sasadmins/trunk admin';
	}
	$host = $_SESSION['zipper']['hostmachine'] -> machine;
endif;
?>
<link rel="stylesheet" href="/SSoD-CSS0003" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0101"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" style="display:none;max-width:1060px;height:100%;border:0px;padding:0px;margin:0 auto;position:relative;">
			<div id="header">
				<a name="home" id="home" href="javascript:void(0);" style="float:left;position:relative;top:-14px;" tabindex="9">Home</a>
				<a name="logout" id="logout" href="javascript:void(0);" style="float:right;position:relative;top:-14px;" tabindex="10">Logout</a>
				<h2 style="float:right;position:relative;top:-28px;padding-right:6px;">ZIPPER</h2>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>
			<div style="max-width:800px;border-style:solid;padding:4px;border-width:1px;border-color:blue;margin:0px auto;display:block;">
				<div style="overflow:auto;padding:10px;max-height:400px;">
					<h3>Exporting the SASAdmin SVN to <?php echo $_SESSION['zipper']['hostmachine'] -> machine; ?></h3>
					<?php
					echo $_SESSION['zipper']['zipper'] -> runcmd($host, $cidowner, $cidpwd, $cmd);
					?>
				</div>
			</div>
			<div style="width:475px;margin:0px auto;">
				<p>After the repository is loaded select <font color="blue">"Next"</font> to begin File System Creation.</p>
				<p>You may also select <font color="blue">"Again"</font> to export the SVN again.</p>
				<button id="redo" class="action bluebtn" tabindex="8" /><span class="label">Again</span></button>
				<button id="mkstruct" class="action bluebtn" tabindex="8" /><span class="label">Next</span></button>
			</div>
		</div>
		<div id="footer">
		    <span style="float:right;">(C) Zipper</span>
		</div>

	</div>
</div>
</body>
</html>
