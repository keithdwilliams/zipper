﻿<?php
if(@filter_has_var(INPUT_POST, 'hostid') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:
	$hostid = @filter_input(INPUT_POST, 'hostid', FILTER_SANITIZE_STRING);
endif;

$_SESSION['zipper']['hostmachine'] = new HostMachine($hostid);
if($_SESSION['zipper']['hostmachine'] == false) {
	echo '02';
	while(@ob_end_flush());
	exit;
}

if(@filter_has_var(INPUT_POST, 'sasv') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:
	$sasv = @filter_input(INPUT_POST, 'sasv', FILTER_SANITIZE_STRING);
	switch ($sasv) {
		case '920':
			$_SESSION['zipper']['hostmachine'] -> sasversion = '920';
			break;
		case '930':
			$_SESSION['zipper']['hostmachine'] -> sasversion = '930';
			break;
		case '940':
			$_SESSION['zipper']['hostmachine'] -> sasversion = '940';
			break;
		default:
			echo '02';
			while(@ob_end_flush());
			exit;		
	}
endif;

if(@filter_has_var(INPUT_POST, 'location') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:

	$location = @filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING);
	switch ($location) {
		case 'cary':
			$_SESSION['zipper']['hostmachine'] -> location = 'cary';
			break;
		case 'wynyard':
			$_SESSION['zipper']['hostmachine'] -> location = 'wynyard';
			break;
		default:
			echo '02';
			while(@ob_end_flush());
			exit;		
	}
endif;

if(@filter_has_var(INPUT_POST, 'mode') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:

	$mode = @filter_input(INPUT_POST, 'mode', FILTER_SANITIZE_STRING);
	switch ($mode) {
		case 'dev':
			$_SESSION['zipper']['hostmachine'] -> mode = 'dev';
			break;
		case 'prod':
			$_SESSION['zipper']['hostmachine'] -> mode = 'prod';
			break;
		default:
			echo '02';
			while(@ob_end_flush());
			exit;		
	}
endif;

if(@filter_has_var(INPUT_POST, 'projid') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:

	$projid = @filter_input(INPUT_POST, 'projid', FILTER_SANITIZE_STRING);
	$_SESSION['zipper']['hostmachine'] -> projectid = strtolower($projid);
endif;

if(@filter_has_var(INPUT_POST, 'ousername') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:

	$ousername = @filter_input(INPUT_POST, 'ousername', FILTER_SANITIZE_STRING);
	$_SESSION['zipper']['hostmachine'] -> ownerid = strtolower($ousername);
endif;

if(@filter_has_var(INPUT_POST, 'opassword') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:

	$opassword = @filter_input(INPUT_POST, 'opassword', FILTER_SANITIZE_STRING);
	$_SESSION['zipper']['hostmachine'] -> ownerpwd = $opassword;
endif;

if(@filter_has_var(INPUT_POST, 'rusername') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:

	$rusername = @filter_input(INPUT_POST, 'rusername', FILTER_SANITIZE_STRING);
	$_SESSION['zipper']['hostmachine'] -> runid = strtolower($rusername);
endif;
echo '01';
while(@ob_end_flush());
exit;
?>