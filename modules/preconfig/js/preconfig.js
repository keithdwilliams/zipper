﻿$(document).ready(function () {


	/*////////////////////
	/////Login Script/////
	////////////////////*/
	
	$('button#start').bind('click', function(e){
		
		/*////////////////////////////////////////////
		/////lets validate data on the login form/////
		////////////////////////////////////////////*/
		
		var reason = '';
		var notice = '';
		var alert = '';
		reason += validateName(this.form.ownerid);
		reason += validatePassword(this.form.ownerpwd);
		reason += validateName(this.form.runid);
		
		/*//////////////////////////////////////////////////
		/////if there is a reason the form is not valid///// 
		/////then we will send a popup with the reason /////
		//////////////////////////////////////////////////*/
		
		if (reason != "") {
			var x = document.getElementById('formAlert');
			x.innerHTML = reason;
			$('#linkAlert').click();
			
			/*////////////////////////////////////////////////
			/////The data in the form appears to be valid/////
			////////////////////////////////////////////////*/

			} else {
			
			/*///////////////////////////////////////////////////////////////////
			/////Lets send the form date to the registration processing page/////
			///////////////////////////////////////////////////////////////////*/
			
			$.ajax({
				type: 'POST',
				url: '/SSoD-0101',
				data: {
					ousername : $('input#ownerid').val(),
					opassword : $('input#ownerpwd').val(),
					rusername : $('input#runid').val(),
					hostid : $('input#host').val(),
					projid : $('input#projid').val(),
					sasv : $('select#sasv').val(),
					location : $('select#location').val(),
					mode : $('select#mode').val(),
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {

					/*//////////////////////////////////
					/////Preventing a double submit/////
					//////////////////////////////////*/
					
					$('button#start').prop('disabled', 'true');
					$('input#host').prop('disabled', 'true');
					$('input#projid').prop('disabled', 'true');
					$('select#sasv').prop('disabled', 'true');
					$('select#location').prop('disabled', 'true');
					$('select#mode').prop('disabled', 'true');
					$('input#ownerid').prop('disabled', 'true');
					$('input#ownerpwd').prop('disabled', 'true');
					$('input#runid').prop('disabled', 'true');
					
					$('div#pptxt').html('Processing Pre-Configure Information ...<br />Please wait.');
					$('#prepage').show();
				},	
				success: function(text){
					
					/*//////////////////////////////////////////////
					/////Create a minimum delay for the spinner/////
					//////////////////////////////////////////////*/
					
					var ms = 1000;
					ms += new Date().getTime();
					while (new Date() < ms){};
					
					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/
					
					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/
					
					switch(text) {
						case '01':
							notice = 'Configuration Information successfully loaded. <br /> Checking SUDO Access';
							window.top.location.href = '/SSoD-0102';
							break;
						case '02':
							alert = 'Configuration Failure';
							break;
						default:
							alert = text;
							break;
					}
				},
				error: function(xhr, status, error){
					var err = eval("(" + xhr.responseText + ")");
					alert = err.Message;
				},
				complete: function() {
					
					/*///////////////////////////////////////////////
					/////enable the inputs since we are finished/////
					///////////////////////////////////////////////*/
					
					$('button#start').prop('disabled', 'false');
					$('input#host').prop('disabled', 'false');
					$('input#projid').prop('disabled', 'false');
					$('select#sasv').prop('disabled', 'false');
					$('select#location').prop('disabled', 'false');
					$('select#mode').prop('disabled', 'false');
					$('input#ownerid').prop('disabled', 'false');
					$('input#ownerpwd').prop('disabled', 'false');
					$('input#runid').prop('disabled', 'false');

					/*///////////////////////////////////
					/////clear the registration form/////
					///////////////////////////////////*/

					$('input#ownerid').val(''),
					$('input#ownerpwd').val(''),
					$('input#runid').val(''),
					$('input#host').val(''),
					$('input#projid').val(''),
					$('select#sasv').val(''),
					$('select#location').val(''),
					$('select#mode').val(''),
					
					/*//////////////////////////////////////////////////
					/////hide the spinner and load the default text/////
					//////////////////////////////////////////////////*/
					
					$('#prepage').hide();
					$('div#pptxt').html('');
					
					/*/////////////////////////////////////////////////////////
					/////load the alert box with our status for the member/////
					/////////////////////////////////////////////////////////*/
					
					$('div#tmpnotice').remove();
					if(notice != '') {
						$('div#alertnotice').append('<div id="tmpnotice" class="noticebox"><sub style="color:green;text-align:left;">'+notice+'</sub></div>');
					}
					if(alert != '') {
						$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">'+alert+'</sub></div>');
					}
				}
			});
		}
		
		/*///////////////////////////////////////////
		/////stop the default action of the form/////
		///////////////////////////////////////////*/
		
		e.preventDefault();
	});
	$('button#redo').bind('click', function(e){
		$('div#pptxt').html('Running again, retrying ...<br />Please wait.');
		$('#prepage').show();
		location.reload(false);
	});
	$('button#ulimit').bind('click', function(e){
		$('div#pptxt').html('Running Command to retieve ulimit ...<br />Please wait.');
		$('#prepage').show();
		window.top.location.href = '/SSoD-0103';
	});
	$('button#setsso').bind('click', function(e){
		$('div#pptxt').html('Setting /sso Directory Permissions ...<br />Please wait.');
		$('#prepage').show();
		window.top.location.href = '/SSoD-0104';
	});
	$('button#loadsvn').bind('click', function(e){
		$('div#pptxt').html('Loading the SASAdmin SVN ...<br />Please wait.');
		$('#prepage').show();
		window.top.location.href = '/SSoD-0105';
	});
	$('button#mkstruct').bind('click', function(e){
		$('div#pptxt').html('Creating the file system ...<br />Please wait.');
		$('#prepage').show();
		window.top.location.href = '/SSoD-0106';
	});
	$('button#junit').bind('click', function(e){
		$('div#pptxt').html('Installing JUnit ...<br />Please wait.');
		$('#prepage').show();
		window.top.location.href = '/SSoD-0107';
	});
	$('button#ssorun').bind('click', function(e){
		$('div#pptxt').html('Installing SSO_RUN ...<br />Please wait.');
		$('#prepage').show();
		window.top.location.href = '/SSoD-0108';
	});
	$('button#finish').bind('click', function(e){
		window.top.location.href = '/SSoD-0109';
	});
	$('a#gomid').bind('click', function(e){
		window.top.location.href = '/SSoD-0200';
	});
	$('a#goagain').bind('click', function(e){
		window.top.location.href = '/SSoD-0100';
	});
	$('a#gohome').bind('click', function(e){
		window.top.location.href = '/SSoD-0002';
	});
	$("input#projid").blur(function(){
		var fillid = $("input#projid").val();
		$('input#ownerid').val(fillid+'owner');
		$('input#ownerid').attr("placeholder", "");
		$('input#runid').val(fillid+'run');
		$('input#runid').attr("placeholder", "");
	});
	$("select").change(function () {
	    if($(this).val() == "") $(this).addClass("empty");
	    else $(this).removeClass("empty")
	});
	
	$("select").change();
});