﻿<?php
if(@filter_has_var(INPUT_POST, 'host') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:
	$host = @filter_input(INPUT_POST, 'host', FILTER_SANITIZE_STRING);
endif;

$_SESSION['zipper']['hostmachine'] = new HostMachine($host);
if($_SESSION['zipper']['hostmachine'] == false) {
	echo '03';
	while(@ob_end_flush());
	exit;
}

if(@filter_has_var(INPUT_POST, 'ownerid') === false) :
	echo '04';
	while(@ob_end_flush());
	exit;
else:

	$ownerid = @filter_input(INPUT_POST, 'ownerid', FILTER_SANITIZE_STRING);
	$_SESSION['zipper']['hostmachine'] -> ownerid = strtolower($ownerid);
endif;

if(@filter_has_var(INPUT_POST, 'ownerpwd') === false) :
	echo '05';
	while(@ob_end_flush());
	exit;
else:

	$ownerpwd = @filter_input(INPUT_POST, 'ownerpwd', FILTER_SANITIZE_STRING);
	$_SESSION['zipper']['hostmachine'] -> ownerpwd = $ownerpwd;
endif;

require($_SERVER['DOCUMENT_ROOT'].'/api/Net/SSH2.php');
$cidowner = $_SESSION['zipper']['hostmachine'] -> ownerid;
$cidpwd = $_SESSION['zipper']['hostmachine'] -> ownerpwd;
$ipaddr = $_SESSION['zipper']['hostmachine'] -> machine;
$connection = ssh2_connect($ipaddr, 22);
ssh2_auth_password($connection, $cidowner, $cidpwd);
if (ssh2_scp_send($connection, '/var/www/software/esd/esdclient__93540__lax__xx__web__1', '/sso/depot/thirdparty/esdclient__93540__lax__xx__web__1h', 0755) === false):
	ssh2_exec($connection, 'exit'); 
	echo '06';
	exit;
else:
	ssh2_exec($connection, 'exit'); 
	echo '01';
	exit;
endif;
