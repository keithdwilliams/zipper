﻿$(document).ready(function () {

	/*//////////////////
	/////ESD Script/////
	//////////////////*/
	
	$('button#start').bind('click', function(e){
		
		/*////////////////////////////////////////////
		/////lets validate data on the login form/////
		////////////////////////////////////////////*/
		
		var reason = '';
		var notice = '';
		var myhtml = '';
		
		/*//////////////////////////////////////////////////
		/////if there is a reason the form is not valid///// 
		/////then we will send a popup with the reason /////
		//////////////////////////////////////////////////*/
		
		if (reason != "") {
			var x = document.getElementById('formAlert');
			x.innerHTML = reason;
			$('#linkAlert').click();
			
			/*////////////////////////////////////////////////
			/////The data in the form appears to be valid/////
			////////////////////////////////////////////////*/

			} else {
			
			/*///////////////////////////////////////////////////////////////////
			/////Lets send the form date to the registration processing page/////
			///////////////////////////////////////////////////////////////////*/
			
			var host = $('input#host').val();
			var ownerid = $('input#ownerid').val();
			var ownerpwd = $('input#ownerpwd').val();
			
			$.ajax({
				type: 'POST',
				url: '/SSoD-0231',
				data: {
					host : host,
					ownerid : ownerid,
					ownerpwd : ownerpwd,
				},
				dataType: 'text',
				beforeSend: function() {

					/*//////////////////////////////////
					/////Preventing a double submit/////
					//////////////////////////////////*/
					
					$('div#pptxt').html('Processing ESD Installation<br />Please wait...');
					$('#prepage').show();
					$('div#dyncontent').html('');
					$('button#start').remove();
				},	
				success: function(text){
					
					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/
					
					text = $.trim(text);
					
					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/
					
					switch(text) {
						case '01':
							/*update content with success*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0008" style="display::inline" /><font color="green">ESD installed successfully<font></div>';
							$('div#dyncontent').append(myhtml);
							break;
						case '02':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display::inline" /><font color="red">JDK install failed</font></div>';
							$('div#dyncontent').append(myhtml);
							break;
						case '03':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display::inline" /><font color="red">JDK install failed</font></div>';
							$('div#dyncontent').append(myhtml);
							break;
						case '04':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display::inline" /><font color="red">JDK install failed</font></div>';
							$('div#dyncontent').append(myhtml);
							break;
						case '05':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display::inline" /><font color="red">JDK install failed</font></div>';
							$('div#dyncontent').append(myhtml);
							break;
						case '06':
							/*update content with failure*/
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display::inline" /><font color="red">JDK install failed</font></div>';
							$('div#dyncontent').append(myhtml);
							break;
						default:
							myhtml='<div style="display:block"><img src="/SSoD-IM0016" style="display::inline" /><font color="red">'+text+'</font></div>';
							$('div#dyncontent').append(myhtml);
							break;
					}					
				},
				error: function(xhr, status, error){
					var err = eval("(" + xhr.responseText + ")");
					alert(err.Message);
				},
				complete: function() {
					$('#prepage').hide();
					$('div#buttonholder').append('<button id="gohome" name="gohome" class="action bluebtn"><span class="label">Home</span></button>');
					$('#prepage').hide();
				}
			});
		}
		
		/*///////////////////////////////////////////
		/////stop the default action of the form/////
		///////////////////////////////////////////*/
		
		e.preventDefault();
		
		/*Adding an event to capture the click of the dynamically created button*/
		
		$('button#gohome').livequery("click", function () {
			window.top.location.href = '/SSoD-0002';
		});
	});
});