$(document).ready(function () {

	if(localStorage.getItem("username") !== null) {
		$('input#vspid').val(localStorage.username);
		setTimeout(function(){
			$('input#vsppwd').focus();
		},300);
	}
	$('input#vspid').blur(function() {
		if($('input#vspid').val() != '') {
			localStorage.username = $('input#vspid').val();
		}
	});


	/*////////////////////
	/////Login Script/////
	////////////////////*/

	$('button#vsplogin').bind('click', function(e){

		/*////////////////////////////////////////////
		/////lets validate data on the login form/////
		////////////////////////////////////////////*/

		var reason = '';
		var notice = '';
		var alert = '';
		reason += validateName(this.form.vspid);
		reason += validatePassword(this.form.vsppwd);

		/*//////////////////////////////////////////////////
		/////if there is a reason the form is not valid/////
		/////then we will send a popup with the reason /////
		//////////////////////////////////////////////////*/

		if (reason != "") {
			var x = document.getElementById('formAlert');
			x.innerHTML = reason;
			$('#linkAlert').click();

			/*////////////////////////////////////////////////
			/////The data in the form appears to be valid/////
			////////////////////////////////////////////////*/

			} else {

			/*////////////////////////////////////////////////////////////
			/////Lets send the form date to the login processing page/////
			/////////////////////////////////////////////////////////////*/

			$.ajax({
				type: 'POST',
				url: '/SSoD-0001',
				data: {
					username : $('input#vspid').val(),
					password : $('input#vsppwd').val(),
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {

					/*//////////////////////////////////
					/////Preventing a double submit/////
					//////////////////////////////////*/

					$('button#vsplogin').prop('disabled', 'true');
					$('input#vspid').prop('disabled', 'true');
					$('input#vsppwd').prop('disabled', 'true');

					$('div#pptxt').html('Processing Login ...<br />Please wait.');
					$('#prepage').show();
				},
				success: function(text){

					/*//////////////////////////////////////////////
					/////Create a minimum delay for the spinner/////
					//////////////////////////////////////////////*/

					var ms = 1000;
					ms += new Date().getTime();
					while (new Date() < ms){};

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							notice = 'Successfully Logged in';
							window.top.location.href = '/SSoD-0002';
							break;
						case '02':
							alert = 'Login Failure';
							break;
						case '03':
							alert = 'Session Timeout';
							window.top.location.href = '/SSoD-0000';
							break;
						default:
							alert = text;
							break;
					}
				},
				error: function(xhr, status, error){
					var err = eval("(" + xhr.responseText + ")");
					alert = err.Message;
				},
				complete: function() {

					/*///////////////////////////////////////////////
					/////enable the inputs since we are finished/////
					///////////////////////////////////////////////*/

					$('button#vsplogin').prop('disabled', false);
					$('input#vspid').prop('disabled', false);
					$('input#vsppwd').prop('disabled', false);

					/*//////////////////////////////////////////////////
					/////hide the spinner and load the default text/////
					//////////////////////////////////////////////////*/

					$('#prepage').hide();
					$('div#pptxt').html('');

					/*/////////////////////////////////////////////////////////
					/////load the alert box with our status for the member/////
					/////////////////////////////////////////////////////////*/

					$('div#tmpnotice').remove();
					if(notice != '') {
						$('div#alertnotice').append('<div id="tmpnotice" class="noticebox"><sub style="color:green;text-align:left;">'+notice+'</sub></div>');
					}
					if(alert != '') {
						$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">'+alert+'</sub></div>');
					}
				}
			});
		}

		/*///////////////////////////////////////////
		/////stop the default action of the form/////
		///////////////////////////////////////////*/

		e.preventDefault();
	});
	$('a#precfg').bind('click', function(e){
		window.top.location.href = '/SSoD-0100';
	});
	$('a#pstcfg').bind('click', function(e){
		window.top.location.href = '/SSoD-0150';
	});
	$('a#midcfg').bind('click', function(e){
		window.top.location.href = '/SSoD-0200';
	});
	$('a#loadesd').bind('click', function(e){
		window.top.location.href = '/SSoD-0230';
	});
	$('a#loadjdk').bind('click', function(e){
		window.top.location.href = '/SSoD-0240';
	});
	$('a#checkHost').bind('click', function(e){
		window.top.location.href = '/SSoD-0310';
	});
	$('a#admin').bind('click', function(e){
		window.top.location.href = '/SSoD-0310';
	});	
        $('a#hard').bind('click', function(e){
		window.top.location.href = '/SSoD-1200';
	});
	$('a#odvs').bind('click', function(e){
		window.top.location.href = '/SSoD-1000';
	});
});