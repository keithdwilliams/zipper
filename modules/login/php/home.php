<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Home');
endif;
?>
<link rel="stylesheet" href="/SSoD-CSS0002" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0010"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" style="display:none;max-width:1060px;height:100%;border:0px;padding:0px;margin:0 auto;position:relative;">
			<div id="header">
				<h2 style="float:right;position:relative;top:-28px;">ZIPPER</h2>
				<a name="logout" id="logout" href="javascript:void(0);">Logout</a>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>
			<div style="float:left;width:470px;border-style:solid;padding:15px;border-width:1px;border-color:blue;margin:3px;">
				<h3>Installation Assistance</h3>
				<ul>
					<li>
						<a href="javascript:void(0);" name="precfg" id="precfg" style="text-decoration:none;">Pre-Configure a new host.</a>
					</li>
					<li>
						<a href="javascript:void(0);" name="pstcfg" id="pstcfg" style="text-decoration:none;"><font color="gray">Post-Configure a new host.</font></a>
					</li>
					<li>
						<a href="javascript:void(0);" name="midcfg" id="midcfg" style="text-decoration:none;">Configure a Middle Tier.</a>
					</li>
					<li>
						<a href="javascript:void(0);" name="loadesd" id="loadesd" style="text-decoration:none;">Upload ESD Client</a>
					</li>
					<li>
						<a href="javascript:void(0);" name="omrcfg" id="omrcfg" style="text-decoration:none;"><font color="gray">Load SASAdmin users into an OMR</font></a>
					</li>
					<li>
						<a href="javascript:void(0);" name="loadjdk" id="loadjdk" style="text-decoration:none;"><font color="gray">Load JDK</font></a>
					</li>
					<li>
						<a href="javascript:void(0);" name="loadjdk" id="loadjdk" style="text-decoration:none;"><font color="gray">Install LSF</font></a>
					</li>
				</ul>
			</div>
			<div style="float:right;width:470px;border-style:solid;padding:15px;border-width:1px;border-color:blue;margin:3px;">
				<h3>Host Management</h3>
				<ul>
					<li>
						<a href="javascript:void(0);" name="renlic" id="renlic" style="text-decoration:none;"><font color="gray">Renew/Update SAS Licenses</font></a>
					</li>
					<li>
						<a href="javascript:void(0);" name="upddfl" id="updlic" style="text-decoration:none;"><font color="gray">Renew/Update Dataflux License</font></a>
					</li>
					<li>
						<a href="javascript:void(0);" name="updlsf" id="updlic" style="text-decoration:none;"><font color="gray">Renew/Update LSF License</font></a>
					</li>
					<li>
						<a href="javascript:void(0);" name="updlic" id="expusers" style="text-decoration:none;"><font color="gray">Export and Import users</font></a>
					</li>
					<li>
						<a href="javascript:void(0);" name="checkHost" id="checkHost" style="text-decoration:none;">Precheck Host</a>
					</li>
				</ul>
			</div>
			<div style="clear:both"></div>
			<div style="float:left;width:470px;border-style:solid;padding:15px;border-width:1px;border-color:blue;margin:3px;">
				<h3>Restart Servers</h3>
				<ul>
					<li>
						<a href="javascript:void(0);" name="precfg" id="sssas" style="text-decoration:none;"><font color="gray">Start/Stop SAS Servers</font></a>
					</li>
					<li>
						<a href="javascript:void(0);" name="pstcfg" id="ssmid" style="text-decoration:none;"><font color="gray">Start/Stop Middle Tier</font></a>
					</li>
					<li>
						<a href="javascript:void(0);" name="midcfg" id="sslsf" style="text-decoration:none;"><font color="gray">Start/Stop LSF</font></a>
					</li>
				</ul>
			</div>
			<div style="float:right;width:470px;border-style:solid;padding:15px;border-width:1px;border-color:blue;margin:3px;">
				<h3>Maintanance</h3>
				<ul>
					<li>
						<a href="javascript:void(0);" name="precfg" id="sssas" style="text-decoration:none;"><font color="gray">Install new order to depot</font></a>
					</li>
					<li>
						<a href="javascript:void(0);" name="precfg" id="sssas" style="text-decoration:none;"><font color="gray">Install Hotfix</font></a>
					</li>
					<li>
						<a href="javascript:void(0);" name="precfg" id="sssas" style="text-decoration:none;"><font color="gray">Add License(s) to depot</font></a>
					</li>
				</ul>
			</div>
			<div style="clear:both;"></div>
			<?php
			if($_SESSION['zipper']['zipper'] -> username === 'eurkew' || $_SESSION['zipper']['zipper'] -> username === 'royoun'):
			echo '<div style="width:470px;border-style:solid;padding:15px;border-width:1px;border-color:blue;margin:3px;">
				<h3>Administration</h3>
				<ul>
					<li>
						<a href="javascript:void(0);" name="admin" id="admin" style="text-decoration:none;"><font color="gray">Zipper Administration</font></a>
                                        </li>
                                        <li>
                                                <a href="javascript:void(0);" name="hard" id="hard" style="text-decoration:none;">JBoss Hardening</font></a>
					</li>
				</ul>
			</div>';
			endif;
			if($_SESSION['zipper']['zipper'] -> username === 'navngu' || $_SESSION['zipper']['zipper'] -> username === 'eurkew'):
			echo '<div style="width:470px;border-style:solid;padding:15px;border-width:1px;border-color:blue;margin:3px;">
				<h3>Administration</h3>
				<ul>
					<li>
						<a href="javascript:void(0);" name="odvs" id="odvs" style="text-decoration:none;"><font color="gray">ODVS Administration</font></a>
					</li>
				</ul>
			</div>';
			endif;
			?>
		</div>
		<div id="footer">
		    (C) Zipper
		</div>

	</div>
</div>
</body>
</html>
