﻿<?php
if(Browser::is_old_browser()):
	echo'
	<style>
	.app_old_browser_notice {
		background-color:#ff0000;
		color: white;
		margin:20px;
		padding:10px;
	}

	.app_old_browser_notice a {
		color: white;
	}
	</style>
	<div class="app_old_browser_notice">
		<h2>Old Browser</h2>
		<h3>Please consider upgrading your browser. Doing so will improve experience with the site.</h3>
		<hr />
		<p>Please click one of them below.</p>
		<ul>
			<li><a href="http://www.mozilla.org/en-US/firefox/fx/" target="_blank">Mozilla Firefox</a></li>
			<li><a href="https://www.google.com/chrome" target="_blank">Google Chrome</a></li>
			<li><a href="http://www.apple.com/safari/" target="_blank">Safari</a></li>
			<li><a href="http://www.opera.com/download/" target="_blank">Opera</a></li>
			<li><a href="http://www.microsoft.com/windows/ie/" target="_blank">Internet Explorer (Windows)</a></li>
		</ul>
	</div>';
else :
	$_SESSION['zipper']['zipper'] = new Zipper();
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Login');
	?>
	<link rel="stylesheet" href="/SSoD-CSS0002" type="text/css" media="screen" />
	<script type="text/javascript" language="javascript" src="/SSoD-JS0010"></script>
	<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
		<!-- Page -->
		<div id="page" style="display:none;max-width:1060px;height:100%;border:0px;padding:0px;margin:0 auto;position:relative;">
				<!-- Header -->
				<div id="header">
					<h2 style="float:right;position:relative;top:-28px;">ZIPPER</h2>
				</div>
			<!-- Content -->
			<div id="content">
			<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>	
				<?php		
				if(isset($_SESSION['zipper']['zipper'] -> loggedin) && $_SESSION['zipper']['zipper'] -> loggedin == '1') {
					header("location:/SSoD-0002");
				} else {
					echo '<form style="text-align:center;">';
						echo '<h4>Login</h4>';
						echo '<div>';
							echo '<input class="name" rel="VSP\Username" title="Enter VSP Username" id="vspid" name="vspid" autocomplete="off" type="text" maxlength="10" tabindex="1" autofocus="autofocus" />';
						echo '</div>';
						echo '<br />';
						echo '<div>';
							echo '<input class="password" rel="Enter Password" title="Enter VSP Password" id="vsppwd" name="vsppwd" autocomplete="off" type="password" maxlength="20"tabindex="2" />';
						echo '</div>';
						echo '<div class="button"  style="padding:8px 0px 0px 0px;margin: 0px auto">';
							echo '<button id="vsplogin" class="action bluebtn" tabindex="3" /><span class="label">Login</span></button>';
						echo '</div>';
					echo '</form>';
				}
				?>
			</div>
			<div id="footer">
			    (C) Footer
			</div>

		</div>
	</div>
	</body>
	</html>
<?php endif; ?>
