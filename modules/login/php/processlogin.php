﻿<?php
if(@filter_has_var(INPUT_POST, 'username') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:

	/*/////////////////////////////////
	/////clean the username string/////
	/////////////////////////////////*/

	$username = strtolower(@filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING));
	$username = str_replace('vsp\\','',$username);
endif;
if(@filter_has_var(INPUT_POST, 'password') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:

	/*/////////////////////////////////
	/////clean the password string/////
	/////////////////////////////////*/

	$password = @filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
endif;

$ldap = ldap_connect("vsp.sas.com");

if($bind = ldap_bind($ldap, 'vsp\\'.$username, $password)) :
	$_SESSION['zipper']['zipper'] -> username = $username;
	$_SESSION['zipper']['zipper'] -> password = $password;
	$_SESSION['zipper']['zipper'] -> loggedin = '1';
	$mysqli = new mysqli(HOSTi, DBUSERi, PASSi, DBi);
	if(mysqli_connect_errno()):
		$mysqli -> close(); 
		while(@ob_end_flush());
		echo '04';
		exit();
	else :
		if($stmt = $mysqli -> prepare("SELECT displayname, role FROM zipper_users WHERE username = ? LIMIT 1")):
			$stmt -> bind_param('s', $username);
			$stmt -> execute();
		    	$stmt -> store_result();
		    	if($stmt -> num_rows != 0):
				$stmt -> bind_result($displayname, $role);
				$stmt -> fetch();
				$_SESSION['zipper']['zipper'] -> displayname = $displayname;
				$_SESSION['zipper']['zipper'] -> role = $role;
				if(empty($_SESSION['zipper']['zipper'] -> displayname)):
					$stmt = $mysqli -> prepare("INSERT INTO zipper_users (displayname) VALUES (?)");
					$stmt -> bind_param('s', $username);
					if(!$stmt -> execute()):
						$stmt -> close();
						echo '05';
						while(@ob_end_flush());
						exit;
					else:
						$_SESSION['zipper']['zipper'] -> displayname = $username;
						$_SESSION['zipper']['zipper'] -> role = '';
					endif;
				endif;
			else:
				$stmt = $mysqli -> prepare("INSERT INTO zipper_users (username, displayname) VALUES (?, ?)");
				$stmt -> bind_param('ss', $username, $username);
				if(!$stmt -> execute()):
					$stmt -> close();
					echo '06';
					while(@ob_end_flush());
					exit;
				endif;
				$userid = $mysqli->insert_id;
				$stmt = $mysqli -> prepare("INSERT INTO zipper_environment (userid, username) VALUES (?, ?)");
				$stmt -> bind_param('ss', $userid, $username);
				if(!$stmt -> execute()):
					$stmt -> close();
					echo '07';
					while(@ob_end_flush());
					exit;
				endif;
				$_SESSION['zipper']['zipper'] -> displayname = $username;
				$_SESSION['zipper']['zipper'] -> role = '';
		    	endif;
		else:
			$stmt = $mysqli -> prepare("INSERT INTO zipper_users (username, displayname) VALUES (?, ?)");
			$stmt -> bind_param('ss', $username, $username);
			if(!$stmt -> execute()):
				$stmt -> close();
				echo '08';
				while(@ob_end_flush());
				exit;
			endif;
			$stmt = $mysqli -> prepare("INSERT INTO zipper_environment (username) VALUES (?)");
			$stmt -> bind_param('s', $username);
			if(!$stmt -> execute()):
				$stmt -> close();
				echo '09';
				while(@ob_end_flush());
				exit;
			endif;
			$_SESSION['zipper']['zipper'] -> displayname = $username;
			$_SESSION['zipper']['zipper'] -> role = '';
		endif;
		$stmt -> close();
		$mysqli -> close();
		echo '01';
		while(@ob_end_flush());
		exit;
	endif;	
else:
	echo '03';
	while(@ob_end_flush());
	exit;
endif;
?>
