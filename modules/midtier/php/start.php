﻿<!-- Verify user and load header -->
<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Start Midtier');
endif;
?>
<!-- Custom CSS and Javascript -->
<link rel="stylesheet" href="/SSoD-CSS0004" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS0201"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" style="display:none;max-width:1060px;height:100%;border:0px;padding:0px;margin:0 auto;position:relative;">
			<div id="header">
				<a name="home" id="home" href="javascript:void(0);" style="float:left;position:relative;top:-14px;" tabindex="9">Home</a>
				<a name="logout" id="logout" href="javascript:void(0);" style="float:right;position:relative;top:-14px;" tabindex="10">Logout</a>
				<h2 style="float:right;position:relative;top:-28px;padding-right:6px;">ZIPPER</h2>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>	
			<div style="width:475px;border-style:solid;padding:4px;border-width:1px;border-color:blue;margin:0px auto;">
				<h3>Middle Tier Configure Information</h3>
				<div id="dyncontent">
					<form style="width:410px;margin: 0px auto;">
						<div>
							<?php
							if(isset($_SESSION['zipper']['hostmachine'] -> machine) && $_SESSION['zipper']['hostmachine'] -> machine != '') :
								echo '<input id="host" name="host" class="hostid config" placeholder="Fully qualified host name" type="text" style="width:405px;margin-bottom:2px;" tabindex="1" autofocus="autofocus" require="required" value="'.$_SESSION['zipper']['hostmachine'] -> machine.'" autocomplete="off" />';
							else:
								echo '<input id="host" name="host" class="hostid config" placeholder="Fully qualified host name" type="text" style="width:405px;margin-bottom:2px;" tabindex="1" autofocus="autofocus" require="required" autocomplete="off" />';
							endif;
							?>
						</div>
						<div style="float:left;">
							<?php
							if(isset($_SESSION['zipper']['hostmachine'] -> ownerid) && $_SESSION['zipper']['hostmachine'] -> ownerid != '') :
								echo '<input id="ownerid" name="ownerid" class="name config" placeholder="CID Owner ID" type="text" tabindex="6" value="'.$_SESSION['zipper']['hostmachine'] -> ownerid.'" require="required" />';
							else:
								echo '<input id="ownerid" name="ownerid" class="name config" placeholder="CID Owner ID" type="text" tabindex="6" value="" require="required" autocomplete="off" />';
							endif;
							?>
						</div>
						<div style="float:right;">
							<?php
							if(isset($_SESSION['zipper']['hostmachine'] -> ownerpwd) && $_SESSION['zipper']['hostmachine'] -> ownerpwd != '') :
								echo '<input id="ownerpwd" name="ownerpwd" class="password config" placeholder="CID Owner Password" type="password" tabindex="7" value="'.$_SESSION['zipper']['hostmachine'] -> ownerpwd.'" require="required" autocomplete="off" />';
							else:
								echo '<input id="ownerpwd" name="ownerpwd" class="password config" placeholder="CID Owner Password" type="password" tabindex="7" require="required" autocomplete="off" />';
							endif;
							?>
						</div>
						<div style="clear:both;"></div>
						
						<div style="float:left;">
							<h4>Select JBoss Version</h4>
							<input type="radio" name="jboss" id="jboss" value="jb1" tabindex="2" />JBoss 4.3 GA<br />
							<input type="radio" name="jboss" id="jboss" value="jb2" tabindex="3" />JBoss 5.1 GA<br />
							<input type="radio" name="jboss" id="jboss" value="jb3" tabindex="4" />JBoss 5.2 EAP
						</div>
						<div style="float:right;">
							<h4>Select JDK Version</h4>
							<input type="radio" name="jsdk" id="jsdk" value="jdk1" tabindex="5" />jdk1.6.0_24-32bit<br />
							<input type="radio" name="jsdk" id="jsdk" value="jdk2" tabindex="6" />jdk1.6.0_30<br />
							<input type="radio" name="jsdk" id="jsdk" value="" tabindex="7" />JDK
						</div>
						<div style="clear:both;"></div>
					</form>
				</div>
			</div>
			<div id="buttonholder">
				<button id="start" class="action bluebtn" tabindex="8"><span class="label">Start</span></button>
			</div>
		</div>
		<div id="footer">
		    <span style="float:right;">(C) Zipper</span>
		</div>
	</div>
</div>
</body>
</html>

