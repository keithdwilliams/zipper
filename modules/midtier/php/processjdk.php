﻿<?php
if(@filter_has_var(INPUT_POST, 'host') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:
	$host = @filter_input(INPUT_POST, 'host', FILTER_SANITIZE_STRING);
endif;

$_SESSION['zipper']['hostmachine'] = new HostMachine($host);
if($_SESSION['zipper']['hostmachine'] == false) {
	echo '03';
	while(@ob_end_flush());
	exit;
}

if(@filter_has_var(INPUT_POST, 'ownerid') === false) :
	echo '04';
	while(@ob_end_flush());
	exit;
else:

	$ownerid = @filter_input(INPUT_POST, 'ownerid', FILTER_SANITIZE_STRING);
	$_SESSION['zipper']['hostmachine'] -> ownerid = strtolower($ownerid);
endif;

if(@filter_has_var(INPUT_POST, 'ownerpwd') === false) :
	echo '05';
	while(@ob_end_flush());
	exit;
else:

	$ownerpwd = @filter_input(INPUT_POST, 'ownerpwd', FILTER_SANITIZE_STRING);
	$_SESSION['zipper']['hostmachine'] -> ownerpwd = $ownerpwd;
endif;

if(@filter_has_var(INPUT_POST, 'jdk') === false) :
	echo '06';
	while(@ob_end_flush());
	exit;
else:
	$jdk = @filter_input(INPUT_POST, 'jdk', FILTER_SANITIZE_STRING);
	switch ($jdk) {
		case 'jdk1':
			installJDK('jdk1.6.0_24-32bit.zip');
			break;
		case 'jdk2':
			installJDK('jdk1.6.0_30.zip');
			break;
		default:
			echo '07';
			while(@ob_end_flush());
			exit;		
	}
endif;
function installJDK($jdkv) {
	require($_SERVER['DOCUMENT_ROOT'].'/api/Net/SSH2.php');
	$cidowner = $_SESSION['zipper']['hostmachine'] -> ownerid;
	$cidpwd = $_SESSION['zipper']['hostmachine'] -> ownerpwd;
	$cmd1 = 'cd /sso/sfw/java;unzip -o '.$jdkv;
	$ipaddr = $_SESSION['zipper']['hostmachine'] -> machine;
	$connection = ssh2_connect($ipaddr, 22);
	ssh2_auth_password($connection, $cidowner, $cidpwd);
	ssh2_scp_send($connection, '/var/www/html/software/jdk/'.$jdkv, '/sso/sfw/java/'.$jdkv, 0755);
	$ssh = new Net_SSH2($ipaddr);
	if (!$ssh->login($cidowner, $cidpwd)) :
		echo '08';
		exit;
	else:
		$output = $ssh->exec($cmd1);
		$charcnt = strlen($output); 
		if($charcnt > 10000):
			echo '01';
			exit;
		else:
			echo '09';
			exit;
		endif;
	endif;
}