<?php
ob_start();
$browser = new Browser();

/*/////////////////////////////
/////will be output as css/////
/////////////////////////////*/

header('Content-type: text/css; charset: UTF-8');

/*//////////////////////////////
/////set an expiration date/////
//////////////////////////////*/

$days_to_cache = 10;
header('Expires: '.gmdate('D, d M Y H:i:s',time() + (60 * 60 * 24 * $days_to_cache)).' GMT');

	
/*/////////////////////////*/
/*//CSS Variables for IE///*/
/*/////////////////////////*/

if($browser -> getBrowser() == Browser::BROWSER_IE):
	echo '
	,input[type="password"], input[type="text"], select, textarea {
		outline:none;
		background-color:#E6E6F6;
		border:1px solid #333;
		-moz-transition: all 0.25s ease-in-out;
		-moz-border-radius: 5px; 
		-webkit-transition: all 0.25s ease-in-out;
		-webkit-border-radius: 5px; 
		transition: all 0.25s ease-in-out;
		border-radius: 5px;
		-o-transition:  all 0.25s ease-in-out;
	}

	input[type="text"]:focus, input[type="password"]:focus, select:focus, textarea:focus {
		background-color:#fff;
		border:2px solid #333;
	}

	input[type="text"]:hover, input[type="password"]:hover, select:hover, textarea:hover {
		background-color:#fff;
		border:2px solid #333;
	}

	input[type="text"]:blur, input[type="password"]:blur, select:blur, textarea:blur {
		outline:none;
		transition: all 0.25s ease-in-out;
		-webkit-transition: all 0.25s ease-in-out;
		-moz-transition: all 0.25s ease-in-out;
		background-color:#E6E6F6;
		border:1px solid #333;
		-webkit-border-radius: 5px; 
		-moz-border-radius: 5px; 
		border-radius: 5px;
	}

	input[type="text"], input[type="password"] {
		padding: 10px 4px 0px 25px;
		height:25px;
		width:160px;
	}';
else:
	echo '
	,input[type="password"], input[type="text"], select, textarea {
		outline:none;
		background-color:#E6E6F6;
		border:1px solid #333;
		-moz-transition: all 0.25s ease-in-out;
		-moz-border-radius: 5px; 
		-webkit-transition: all 0.25s ease-in-out;
		-webkit-border-radius: 5px; 
		transition: all 0.25s ease-in-out;
		border-radius: 5px;
		-o-transition:  all 0.25s ease-in-out;
	}

	input[type="text"]:focus, input[type="password"]:focus, select:focus, textarea:focus {
		background-color:#fff;
		border:2px solid #333;
	}

	input[type="text"]:hover, input[type="password"]:hover, select:hover, textarea:hover {
		background-color:#fff;
		border:2px solid #333;
	}

	input[type="text"]:blur, input[type="password"]:blur, select:blur, textarea:blur {
		outline:none;
		transition: all 0.25s ease-in-out;
		-webkit-transition: all 0.25s ease-in-out;
		-moz-transition: all 0.25s ease-in-out;
		background-color:#E6E6F6;
		border:1px solid #333;
		-webkit-border-radius: 5px; 
		-moz-border-radius: 5px; 
		border-radius: 5px;
	}

	input[type="text"], input[type="password"] {
		padding: 10px 4px 0px 25px;
		height:35px;
		width:200px;
	}';

endif;
while(@ob_end_flush());
?>