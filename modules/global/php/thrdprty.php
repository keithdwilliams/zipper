﻿<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
endif;
?>
<a href="javascript:void(0);" id="closemenu" name="closemenu"><font color="red">Close</font></a>
<div style="width:100%;text-align:center;">
	<h3>3rd Party Settings</h3>
</div>
<?php
$mysqli = new mysqli(HOSTi, DBUSERi, PASSi, DBi);
if(mysqli_connect_errno()):
	echo '
	<dl>
		<dt><label>JBoss Directory</label><dt>
			<dd><input id="tpdir1" type="text" placeholder="Enter Full Directory" /></dd>
		<dt><label>JDK Directory</label><dt>
			<dd><input id="tpdir2" type="text" placeholder="Enter Full Directory" /></dd>
		<dt><label>LSF Directory</label><dt>
			<dd><input id="tpdir3" type="text" placeholder="Enter Full Directory" /></dd>
		<dt><label>PM Directory</label><dt>
			<dd><input id="tpdir4" type="text" placeholder="Enter Full Directory" /></dd>
	</dl>';
else:
	if($stmt = $mysqli -> prepare("SELECT jboss, jdk, lsf, pm FROM zipper_environment WHERE username = ? LIMIT 1")):
		$stmt -> bind_param('s', $_SESSION['zipper']['zipper'] -> username);
		$stmt -> execute();
		$stmt -> store_result();
		if($stmt -> num_rows != 0):
			$stmt -> bind_result($jboss, $jdk, $lsf, $pm);
			$stmt -> fetch();
			echo '
			<dl>
				<dt><label>JBoss Directory</label><dt>
					<dd><input id="tpdir1" type="text" placeholder="Enter Full Directory" value = "'.$jboss.'" /></dd>
				<dt><label>JDK Directory</label><dt>
					<dd><input id="tpdir2" type="text" placeholder="Enter Full Directory" value = "'.$jdk.'" /></dd>
				<dt><label>LSF Directory</label><dt>
					<dd><input id="tpdir3" type="text" placeholder="Enter Full Directory" value = "'.$lsf.'" /></dd>
				<dt><label>PM Directory</label><dt>
					<dd><input id="tpdir4" type="text" placeholder="Enter Full Directory" value = "'.$pm.'" /></dd>
			</dl>';
		else:
			echo '
			<dl>
				<dt><label>JBoss Directory</label><dt>
					<dd><input id="tpdir1" type="text" placeholder="Enter Full Directory" /></dd>
				<dt><label>JDK Directory</label><dt>
					<dd><input id="tpdir2" type="text" placeholder="Enter Full Directory" /></dd>
				<dt><label>LSF Directory</label><dt>
					<dd><input id="tpdir3" type="text" placeholder="Enter Full Directory" /></dd>
				<dt><label>PM Directory</label><dt>
					<dd><input id="tpdir4" type="text" placeholder="Enter Full Directory" /></dd>
			</dl>';
		endif;
		$stmt -> close();
		$mysqli -> close();
	else:
		echo '
		<dl>
			<dt><label>JBoss Directory</label><dt>
				<dd><input id="tpdir1" type="text" placeholder="Enter Full Directory" /></dd>
			<dt><label>JDK Directory</label><dt>
				<dd><input id="tpdir2" type="text" placeholder="Enter Full Directory" /></dd>
			<dt><label>LSF Directory</label><dt>
				<dd><input id="tpdir3" type="text" placeholder="Enter Full Directory" /></dd>
			<dt><label>PM Directory</label><dt>
				<dd><input id="tpdir4" type="text" placeholder="Enter Full Directory" /></dd>
		</dl>';
	endif;
endif;
?>
<div style="width:100%;text-align:center;">
	<button id="savetp" name="savetp" class="action greenbtn" tabindex="3"><span class="label">Save</span></button>
	<button id="rsttp" name="rsttp" class="action greenbtn" tabindex="3"><span class="label">Reset</span></button>
</div>
<script type="text/javascript" language="javascript">
	$('a#closemenu').bind('click', function(e){
		$.get("/SSoD-GLBL0003", function(data){
			$("div#topmenu").html(data);
		});	
		$('div#topmenu').css('width', '250px');
	});
	$('button#savetp').bind('click', function(e){
		var notice = '';
		var alert = '';
		$.ajax({
			type: 'POST',
			url: '/SSoD-GLBL0001',
			data: {
				tpdir1 : $('input#tpdir1').val(),
				tpdir2 : $('input#tpdir2').val(),
				tpdir3 : $('input#tpdir3').val(),
				tpdir4 : $('input#tpdir4').val(),
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {

				/*//////////////////////////////////
				/////Preventing a double submit/////
				//////////////////////////////////*/

				$('button#savetp').prop('disabled', 'true'); 
				$('button#rsttp').prop('disabled', 'true'); 
				$('div#pptxt').html('Saving ...<br />Please wait.'); 
				$('#prepage').show();
			}, 
			success: function(text){

				/*//////////////////////////////////////////////
				/////Create a minimum delay for the spinner/////
				//////////////////////////////////////////////*/

				var ms = 1000;
				ms += new Date().getTime();
				while (new Date() < ms){};

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						notice = 'Successfully Saved';
						break;
					case '02':
						alert = 'Save Failed';
						break;
					case '03':
						alert = 'Save Failed';
						break;
					default:
						alert = text;
						break;
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log(xhr.responseText);
			},
			complete: function() {

				/*///////////////////////////////////////////////
				/////enable the inputs since we are finished/////
				///////////////////////////////////////////////*/

				$('button#savetp').prop('disabled', false);
				$('button#rsttp').prop('disabled', false);

				/*//////////////////////////////////////////////////
				/////hide the spinner and load the default text/////
				//////////////////////////////////////////////////*/

				$('#prepage').hide();
				$('div#pptxt').html('');

				/*/////////////////////////////////////////////////////////
				/////load the alert box with our status for the member/////
				/////////////////////////////////////////////////////////*/

				$('div#tmpnotice').remove();
				if(notice != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="noticebox"><sub style="color:green;text-align:left;">'+notice+'</sub></div>');
				}
				if(alert != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">'+alert+'</sub></div>');
				}
			}
		});
	});
	$('button#rsttp').bind('click', function(e){
		var notice = '';
		var alert = '';
		$.ajax({
			type: 'POST',
			url: '/SSoD-GLBL0010',
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {

				/*//////////////////////////////////
				/////Preventing a double submit/////
				//////////////////////////////////*/

				$('button#savetp').prop('disabled', 'true');
				$('button#rsttp').prop('disabled', 'true');
				$('div#pptxt').html('Resetting ...<br />Please wait.');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////
				/////Create a minimum delay for the spinner/////
				//////////////////////////////////////////////*/

				var ms = 1000;
				ms += new Date().getTime();
				while (new Date() < ms){};

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						notice = 'Successfully Reset';
						$.get("/SSoD-GLBL0002", function(data){
							$("div#topmenu").html(data);
						});
						break;
					case '02':
						alert = 'Reset Failed';
						break;
					case '03':
						alert = 'Reset Failed';
						break;
					default:
						alert = text;
						break;
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log(xhr.responseText);
			},
			complete: function() {

				/*///////////////////////////////////////////////
				/////enable the inputs since we are finished/////
				///////////////////////////////////////////////*/

				$('button#savetp').prop('disabled', false);
				$('button#rsttp').prop('disabled', false);

				/*//////////////////////////////////////////////////
				/////hide the spinner and load the default text/////
				//////////////////////////////////////////////////*/

				$('#prepage').hide();
				$('div#pptxt').html('');

				/*/////////////////////////////////////////////////////////
				/////load the alert box with our status for the member/////
				/////////////////////////////////////////////////////////*/

				$('div#tmpnotice').remove();
				if(notice != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="noticebox"><sub style="color:green;text-align:left;">'+notice+'</sub></div>');
				}
				if(alert != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">'+alert+'</sub></div>');
				}
			}
		});
	});
</script>