﻿<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
endif;
?>
<!doctype html>
<html>
<head>
	<title><?=(isset($this->title)) ? $this->title : 'soshellite.com'; ?></title>
	<link rel="stylesheet" href="<?php echo URL; ?>/public/css/bootstrap.css" />    
	<link rel="stylesheet" href="<?php echo URL; ?>/public/css/default.css" />    
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/sunny/jquery-ui.css" />
	<?php echo $css; ?>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo URL; ?>/public/js/custom.js"></script>
	<script type="text/javascript" src="<?php echo URL; ?>/public/js/bootstrap.js"></script>
        <?php echo $js; ?>
        

</head>
<body>

  
<div id="header">
    <h2 style="float:right;position:relative;top:-18px;">soshellite.com</h2>
    <?php if (Session::get('loggedIn') == true):?>
        <a href="http://soshellite.com/index.php?controller=dashboard">Dashboard</a>
        <a href="<?php echo URL; ?>note">Notes</a>
        
        <?php if (Session::get('role') == 'owner'):?>
        <a href="<?php echo URL; ?>user">Users</a>
        <?php endif; ?>
        
        <a href="<?php echo URL; ?>dashboard/logout">Logout</a>    
    <?php else: ?>
        <a href="http://soshellite.com/member/login">Login</a>
        <a href="http://soshellite.com/member/register">Register</a>
    <?php endif; ?>
</div>
    
<div id="content">
    
    