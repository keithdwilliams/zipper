﻿<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
endif;
?>
<a href="javascript:void(0);" id="closemenu" name="closemenu"><font color="red">Close</font></a>
<div style="width:100%;text-align:center;">
	<h3>Account Settings</h3>
</div>
<hr />
<dl>
	<dt><label>Displayname</label><dt>
		<dd>
			<input placeholder="Enter your display name" id="dsplynm" type="text" />
			<input id="username" type="hidden" value="<?php echo $_SESSION['zipper']['zipper'] -> username; ?>" />
		</dd>
</dl>
<div style="width:100%;text-align:center;">
	<button id="saveactinf" name="saveactinf" class="action greenbtn" tabindex="3"><span class="label">Save</span></button>
	<button id="rstactinf" name="saveactinf" class="action greenbtn" tabindex="3"><span class="label">Reset</span></button>
</div>
<script type="text/javascript" language="javascript">
	$('a#closemenu').bind('click', function(e){
		$.get("/SSoD-GLBL0003", function(data){
			$("div#topmenu").html(data);
		});	
		$('div#topmenu').css('width', '250px');
	});
	$('button#saveactinf').bind('click', function(e){
		var notice = '';
		var alert = '';
		$.ajax({
			type: 'POST',
			url: '/SSoD-GLBL0006',
			data: {
				dsplynm : $('input#dsplynm').val(),
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {

				/*//////////////////////////////////
				/////Preventing a double submit/////
				//////////////////////////////////*/

				$('button#saveactinf').prop('disabled', 'true'); 
				$('button#rstactinf').prop('disabled', 'true'); 
				$('div#pptxt').html('Saving ...<br />Please wait.'); 
				$('#prepage').show();
			}, 
			success: function(text){

				/*//////////////////////////////////////////////
				/////Create a minimum delay for the spinner/////
				//////////////////////////////////////////////*/

				var ms = 1000;
				ms += new Date().getTime();
				while (new Date() < ms){};

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						notice = 'Successfully Saved';
						$('div#topdiv').html($('input#dsplynm').val());
						break;
					case '02':
						alert = 'Save Failed';
						break;
					case '03':
						alert = 'Save Failed';
						break;
					default:
						alert = text;
						break;
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log(xhr.responseText);
			},
			complete: function() {

				/*///////////////////////////////////////////////
				/////enable the inputs since we are finished/////
				///////////////////////////////////////////////*/

				$('button#saveactinf').prop('disabled', false);
				$('button#rstactinf').prop('disabled', false);

				/*//////////////////////////////////////////////////
				/////hide the spinner and load the default text/////
				//////////////////////////////////////////////////*/

				$('#prepage').hide();
				$('div#pptxt').html('');

				/*/////////////////////////////////////////////////////////
				/////load the alert box with our status for the member/////
				/////////////////////////////////////////////////////////*/

				$('div#tmpnotice').remove();
				if(notice != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="noticebox"><sub style="color:green;text-align:left;">'+notice+'</sub></div>');
				}
				if(alert != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">'+alert+'</sub></div>');
				}
			}
		});
	});
	$('button#rstactinf').bind('click', function(e){
		var notice = '';
		var alert = '';
		$.ajax({
			type: 'POST',
			url: '/SSoD-GLBL0008',
			data: {
				dsplynm : $('input#dsplynm').val(),
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {

				/*//////////////////////////////////
				/////Preventing a double submit/////
				//////////////////////////////////*/

				$('button#saveenv').prop('disabled', 'true'); 
				$('button#rstenv').prop('disabled', 'true'); 
				$('div#pptxt').html('Resetting ...<br />Please wait.'); 
				$('#prepage').show();
			}, 
			success: function(text){

				/*//////////////////////////////////////////////
				/////Create a minimum delay for the spinner/////
				//////////////////////////////////////////////*/

				var ms = 1000;
				ms += new Date().getTime();
				while (new Date() < ms){};

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						notice = 'Successfully Reset';
						$('div#topdiv').html($('input#username').val());
						break;
					case '02':
						alert = 'Save Failed';
						break;
					case '03':
						alert = 'Save Failed';
						break;
					default:
						alert = text;
						break;
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log(xhr.responseText);
			},
			complete: function() {

				/*///////////////////////////////////////////////
				/////enable the inputs since we are finished/////
				///////////////////////////////////////////////*/

				$('button#saveenv').prop('disabled', false);
				$('button#rstenv').prop('disabled', false);

				/*//////////////////////////////////////////////////
				/////hide the spinner and load the default text/////
				//////////////////////////////////////////////////*/

				$('#prepage').hide();
				$('div#pptxt').html('');

				/*/////////////////////////////////////////////////////////
				/////load the alert box with our status for the member/////
				/////////////////////////////////////////////////////////*/

				$('div#tmpnotice').remove();
				if(notice != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="noticebox"><sub style="color:green;text-align:left;">'+notice+'</sub></div>');
				}
				if(alert != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">'+alert+'</sub></div>');
				}
			}
		});
	});
</script>