﻿<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
endif;
$mysqli = new mysqli(HOSTi, DBUSERi, PASSi, DBi);
if(mysqli_connect_errno()):
	$mysqli -> close(); 
	while(@ob_end_flush());
	echo '02';
	exit();
else:
	if(@filter_has_var(INPUT_POST, 'dsplynm') === false) :
		echo '03';
		while(@ob_end_flush());
		exit;
	else:
	
		/*////////////////////////////////////
		/////clean the displayname string/////
		////////////////////////////////////*/
	
		$dsplynm = @filter_input(INPUT_POST, 'dsplynm', FILTER_SANITIZE_STRING);
	endif;
	if($stmt = $mysqli -> prepare("UPDATE zipper_users SET displayname = ? WHERE username = ? LIMIT 1")):
		$stmt -> bind_param('ss',$dsplynm, $_SESSION['zipper']['zipper'] -> username);
		$stmt -> execute();
		$_SESSION['zipper']['zipper'] -> displayname = $dsplynm;
	else:
		echo '04';
		while(@ob_end_flush());
		exit;
	endif;
	$stmt -> close();
	$mysqli -> close();
	echo '01';
	while(@ob_end_flush());
	exit;
endif;
?>