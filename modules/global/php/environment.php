﻿<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
endif;
?>
<a href="javascript:void(0);" id="closemenu" name="closemenu"><font color="red">Close</font></a>
<div style="width:100%;text-align:center;">
	<h3>Global Environment Settings</h3>
</div>
<hr />
<div style="width:100%;text-align:center;">
	<h4>Preset Directories</h4>
</div>
<div style="width:100%;text-align:center;">
	<span style="float:left;">Select default setting:</span>
	<div style="clear:left;"></div>
	<input type="radio" name="envpreset" id="envpreset" value="920" tabindex="2" />SSO 9.2
	<input type="radio" name="envpreset" id="envpreset" value="930" tabindex="3" />SSO 9.3<br />
	<input type="radio" name="envpreset" id="envpreset" value="940" tabindex="4" />SSO 9.4
	<input type="radio" name="envpreset" id="envpreset" value="custom" tabindex="4" />Custom
</div>
<hr />
<?php
$mysqli = new mysqli(HOSTi, DBUSERi, PASSi, DBi);
if(mysqli_connect_errno()):
	echo '
	<dl>
		<dt><label>Depot Directory</label><dt>
			<dd><input id="envdir1" type="text" placeholder="Enter Full Directory" /></dd>
		<dt><label>Configuration Directory</label><dt>
			<dd><input id="envdir2" type="text" placeholder="Enter Full Directory" /></dd>
		<dt><label>SASHome Directory</label><dt>
			<dd><input id="envdir3" type="text" placeholder="Enter Full Directory" /></dd>
		<dt><label>Admin Directory</label><dt>
			<dd><input id="envdir4" type="text" placeholder="Enter Full Directory" /></dd>
	</dl>';
else:
	if($stmt = $mysqli -> prepare("SELECT depot, config, sashome, jboss, jdk, admin FROM zipper_environment WHERE username = ? LIMIT 1")):
		$stmt -> bind_param('s', $_SESSION['zipper']['zipper'] -> username);
		$stmt -> execute();
		$stmt -> store_result();
		if($stmt -> num_rows != 0):
			$stmt -> bind_result($depot, $config, $sashome, $jboss, $jdk, $admin);
			$stmt -> fetch();
			echo '
			<dl>
				<dt><label>Depot Directory</label><dt>
					<dd><input id="envdir1" type="text" placeholder="Enter Full Directory" value = "'.$depot.'" /></dd>
				<dt><label>Configuration Directory</label><dt>
					<dd><input id="envdir2" type="text" placeholder="Enter Full Directory" value = "'.$config.'" /></dd>
				<dt><label>SASHome Directory</label><dt>
					<dd><input id="envdir3" type="text" placeholder="Enter Full Directory" value = "'.$sashome.'" /></dd>
				<dt><label>Admin Directory</label><dt>
					<dd><input id="envdir4" type="text" placeholder="Enter Full Directory" value = "'.$admin.'" /></dd>
			</dl>';
		else:
			echo '
			<dl>
				<dt><label>Depot Directory</label><dt>
					<dd><input id="envdir1" type="text" placeholder="Enter Full Directory" /></dd>
				<dt><label>Configuration Directory</label><dt>
					<dd><input id="envdir2" type="text" placeholder="Enter Full Directory" /></dd>
				<dt><label>SASHome Directory</label><dt>
					<dd><input id="envdir3" type="text" placeholder="Enter Full Directory" /></dd>
				<dt><label>Admin Directory</label><dt>
					<dd><input id="envdir4" type="text" placeholder="Enter Full Directory" /></dd>
			</dl>';
		endif;
		$stmt -> close();
		$mysqli -> close();
	else:
		echo '
		<dl>
			<dt><label>Depot Directory</label><dt>
				<dd><input id="envdir1" type="text" placeholder="Enter Full Directory" /></dd>
			<dt><label>Configuration Directory</label><dt>
				<dd><input id="envdir2" type="text" placeholder="Enter Full Directory" /></dd>
			<dt><label>SASHome Directory</label><dt>
				<dd><input id="envdir3" type="text" placeholder="Enter Full Directory" /></dd>
			<dt><label>Admin Directory</label><dt>
				<dd><input id="envdir4" type="text" placeholder="Enter Full Directory" /></dd>
		</dl>';
	endif;
endif;
?>
<div style="width:100%;text-align:center;">
	<button id="saveenv" name="saveenv" class="action greenbtn" tabindex="3"><span class="label">Save</span></button>
	<button id="rstenv" name="saveenv" class="action greenbtn" tabindex="3"><span class="label">Reset</span></button>
</div>
<script type="text/javascript" language="javascript">
	$('a#closemenu').bind('click', function(e){
		$.get("/SSoD-GLBL0003", function(data){
			$("div#topmenu").html(data);
		});	
		$('div#topmenu').css('width', '250px');
	});
	$('input[type=radio][name=envpreset]').change(function() {
		if (this.value == '920') {
			$('input#envdir1').val('/sso/depot');
			$('input#envdir2').val('/sso/biconfig');
			$('input#envdir3').val('/sso/sfw/sas/920');
			$('input#envdir4').val('/sso/admin');
		} else if (this.value == '930') {
			$('input#envdir1').val('/sso/depot');
			$('input#envdir2').val('/sso/biconfig/930');
			$('input#envdir3').val('/sso/sfw/sas/930');
			$('input#envdir4').val('/sso/admin');
		} else if (this.value == '940') {
			$('input#envdir1').val('/sso/depot');
			$('input#envdir2').val('/sso/biconfig/940');
			$('input#envdir3').val('/sso/sfw/sas/940');
			$('input#envdir4').val('/sso/admin');
		} else if (this.value == 'custom') {
			$('input#envdir1').val('');
			$('input#envdir2').val('');
			$('input#envdir3').val('');
			$('input#envdir4').val('');
		}
	});	
	$('button#saveenv').bind('click', function(e){
		var notice = '';
		var alert = '';
		$.ajax({
			type: 'POST',
			url: '/SSoD-GLBL0007',
			data: {
				envdir1 : $('input#envdir1').val(),
				envdir2 : $('input#envdir2').val(),
				envdir3 : $('input#envdir3').val(),
				envdir4 : $('input#envdir4').val(),
			},
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {

				/*//////////////////////////////////
				/////Preventing a double submit/////
				//////////////////////////////////*/

				$('button#saveenv').prop('disabled', 'true'); 
				$('button#rstenv').prop('disabled', 'true'); 
				$('div#pptxt').html('Saving ...<br />Please wait.'); 
				$('#prepage').show();
			}, 
			success: function(text){

				/*//////////////////////////////////////////////
				/////Create a minimum delay for the spinner/////
				//////////////////////////////////////////////*/

				var ms = 1000;
				ms += new Date().getTime();
				while (new Date() < ms){};

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						notice = 'Successfully Saved';
						break;
					case '02':
						alert = 'Save Failed';
						break;
					case '03':
						alert = 'Save Failed';
						break;
					default:
						alert = text;
						break;
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log(xhr.responseText);
			},
			complete: function() {

				/*///////////////////////////////////////////////
				/////enable the inputs since we are finished/////
				///////////////////////////////////////////////*/

				$('button#saveenv').prop('disabled', false);
				$('button#rstenv').prop('disabled', false);

				/*//////////////////////////////////////////////////
				/////hide the spinner and load the default text/////
				//////////////////////////////////////////////////*/

				$('#prepage').hide();
				$('div#pptxt').html('');

				/*/////////////////////////////////////////////////////////
				/////load the alert box with our status for the member/////
				/////////////////////////////////////////////////////////*/

				$('div#tmpnotice').remove();
				if(notice != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="noticebox"><sub style="color:green;text-align:left;">'+notice+'</sub></div>');
				}
				if(alert != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">'+alert+'</sub></div>');
				}
			}
		});
	});
	$('button#rstenv').bind('click', function(e){
		var notice = '';
		var alert = '';
		$.ajax({
			type: 'POST',
			url: '/SSoD-GLBL0009',
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {

				/*//////////////////////////////////
				/////Preventing a double submit/////
				//////////////////////////////////*/

				$('button#saveenv').prop('disabled', 'true');
				$('button#rstenv').prop('disabled', 'true');
				$('div#pptxt').html('Resetting ...<br />Please wait.');
				$('#prepage').show();
			},	
			success: function(text){

				/*//////////////////////////////////////////////
				/////Create a minimum delay for the spinner/////
				//////////////////////////////////////////////*/

				var ms = 1000;
				ms += new Date().getTime();
				while (new Date() < ms){};

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						notice = 'Successfully Reset';
						$.get("/SSoD-GLBL0004", function(data){
							$("div#topmenu").html(data);
						});
						break;
					case '02':
						alert = 'Reset Failed';
						break;
					case '03':
						alert = 'Reset Failed';
						break;
					default:
						alert = text;
						break;
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log(xhr.responseText);
			},
			complete: function() {

				/*///////////////////////////////////////////////
				/////enable the inputs since we are finished/////
				///////////////////////////////////////////////*/

				$('button#saveenv').prop('disabled', false);
				$('button#rstenv').prop('disabled', false);

				/*//////////////////////////////////////////////////
				/////hide the spinner and load the default text/////
				//////////////////////////////////////////////////*/

				$('#prepage').hide();
				$('div#pptxt').html('');

				/*/////////////////////////////////////////////////////////
				/////load the alert box with our status for the member/////
				/////////////////////////////////////////////////////////*/

				$('div#tmpnotice').remove();
				if(notice != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="noticebox"><sub style="color:green;text-align:left;">'+notice+'</sub></div>');
				}
				if(alert != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">'+alert+'</sub></div>');
				}
			}
		});
	});
</script>