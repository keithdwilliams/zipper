﻿<?php


//////////////////////////////////////
/////will be output as javascript/////
//////////////////////////////////////

header("content-type: application/x-javascript; charset: UTF-8");

////////////////////////////////
/////set an expiration date/////
////////////////////////////////

$days_to_cache = 10;
header('Expires: '.gmdate('D, d M Y H:i:s',time() + (60 * 60 * 24 * $days_to_cache)).' GMT');

echo "function waitPreloadPage() {\n";
	echo "$('#page').fadeIn('slow', function(){\n";
		echo "$('#prepage').fadeOut('slow', function() {\n";
		echo "});\n";
	echo "});\n";
echo "}\n";

?>