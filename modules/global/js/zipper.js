﻿function validateName(fld) {
	var error = "";
	if (fld.value == "") {
		fld.style.backgroundColor = 'Yellow'; 
		error += "User ID is empty<br />";
	} else if ((fld.value.length < 6) || (fld.value.length > 11)) {
		fld.style.backgroundColor = 'Yellow'; 
		error += "User Length is wrong<br />";
	} else {
		fld.style.backgroundColor = 'White';
	}
	return error;
}

function validatePassword(fld) {
	var error = "";
	var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
	if (fld.value == "") {
		fld.style.backgroundColor = 'Yellow';
		error += "Password is empty<br />";
	} else if ((fld.value.length < 5) || (fld.value.length > 15)) {
		error += "Illegal Password<br />";
		fld.style.backgroundColor = 'Yellow';
	} else if (illegalChars.test(fld.value)) {
		error += "Illegal Characters in Password<br />";
		fld.style.backgroundColor = 'Yellow';
	} else {
		fld.style.backgroundColor = 'White';
	}
	return error;
} 

function validatePassword(fld) {
	var error = "";
	var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
	if (fld.value == "") {
		fld.style.backgroundColor = 'Yellow';
		error += "Password is empty<br />";
	} else if ((fld.value.length < 5) || (fld.value.length > 15)) {
		error += "Illegal Password<br />";
		fld.style.backgroundColor = 'Yellow';
	} else if (illegalChars.test(fld.value)) {
		error += "Illegal Characters in Password<br />";
		fld.style.backgroundColor = 'Yellow';
	} else {
		fld.style.backgroundColor = 'White';
	}
	return error;
} 

function validateProject(fld) {
	var error = '';
	var letters = '/^[A-Za-z]+$/'; 
	if (!fld.value.match(letters)) {
		error += "Illegal Characters in Project<br />";
		fld.style.backgroundColor = 'Yellow';
	} else {
		fld.style.backgroundColor = 'White';
	}
	return error;
}

function validateHost(domain) {
	var error = '';
	var re = new RegExp("^[a-z0-9-_]+(\.[a-z0-9-_]+)*\.([a-z]{2,4})$"); 
	if (domain.value == "") {
		domain.style.backgroundColor = 'Yellow';
		error += "Host is empty<br />";
	} else if(!domain.value.match(re)) {
		error += "Illegal Characters in Host Name<br />";
		domain.style.backgroundColor = 'Yellow';
	} else {
		domain.style.backgroundColor = 'White';
	}
	return error;
}

$(document).ready(function(){
	var popID;
	//When you click on a link with class of poplight and the href starts with a # 
	$('a.poplight[href^=#]').live('click',function() {
		popID = $(this).attr('rel'); //Get Popup Name
		var popURL = $(this).attr('href'); //Get Popup href to define size				

		//Pull Query & Variables from href URL
		var query= popURL.split('?');
		var dim= query[1].split('&');
		var popWidth = dim[0].split('=')[1]; //Gets the first query string value

		//Fade in the Popup and add close button
		$('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close"><img src="/SSoD-IM0016" class="btn_close" title="Close Window" alt="Close" /></a>');		

		//Define margin for center alignment (vertical + horizontal) - we add 80 to the height/width to accomodate for the padding + border width defined in the css
		var popMargTop = ($('#' + popID).height() + 80) / 2;
		var popMargLeft = ($('#' + popID).width() + 80) / 2;		

		//Apply Margin to Popup
		$('#' + popID).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});		

		//Fade in Background
		$('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
		$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer 		

		return false;
	});	

	//Close Popups and Fade Layer
	$('a.close, #closeWindow').live('click', function() { //When clicking on the close or fade layer...
	  	$('#fade , .popup_block, .popup_errorblock').fadeOut(function() {
			$('#fade, a.close').remove();  
			$('iframe#poptext').attr('src', '');
	}); //fade them both out
		return false;
	});

	//////////////////////////////////////////////////////////////
	/////Automatically Adjust, create the label and watermark/////
	//////////////////////////////////////////////////////////////

	var i=document.getElementsByTagName('input');
	var a = new Array();
	for(x = 0; x < i.length; x++) {
		if(i[x].hasAttribute('rel')) {
			a[x] = i[x].getAttribute('rel');
			var idx = '#' + i[x].getAttribute('id');
			$(idx).watermark(a[x]);
		}
	}	
	$.get("/SSoD-GLBL0003", function(data){
		$("div#topmenu").html(data);
	});	
	$('a#closemenu').livequery("click", function () {
		$.get("/SSoD-GLBL0003", function(data){
			$("div#topmenu").html(data);
		});
		$('div#topmenu').css('width', '200px');
	});
	$('a#envset').livequery("click", function () {
		$.get("/SSoD-GLBL0004", function(data){
			$('div#topmenu').css('width', '250px');
			$('div#topdiv').css('border-bottom', '0px');
			$("div#topmenu").html(data);
		});	
	});
	$('a#thrdprty').livequery("click", function () {
		$.get("/SSoD-GLBL0002", function(data){
			$('div#topmenu').css('width', '250px');
			$('div#topdiv').css('border-bottom', '0px');
			$("div#topmenu").html(data);
		});	
	});
	$('a#acctinfo').livequery("click", function () {
		$.get("/SSoD-GLBL0005", function(data){
			$('div#topmenu').css('width', '250px');
			$('div#topdiv').css('border-bottom', '0px');
			$("div#topmenu").html(data);
		});	
	});
	$('a#logout').livequery("click", function () {
		window.top.location.href = '/SSoD-0003';
	});
	$('a#home').livequery("click", function () {
		window.top.location.href = '/SSoD-0002';
	});
	$('div#topdiv').livequery("click", function () {
		if($('div#topmenu').attr('val') === 'closed') {
			$('div#topdiv').css('border-bottom', '0px');
			$('div#topmenu').attr('val', 'open');
			$('div#topdiv').css('backgroundImage', 'url(/SSoD-IM0010)');
			$('div#topmenu').fadeIn('slow', function(){
			});
		} else if($('div#topmenu').attr('val') === 'open'){
			$('div#topdiv').css('border-bottom', '4px solid #436fac');
			$('div#topmenu').attr('val', 'closed');
			$('div#topdiv').css('backgroundImage', 'url(/SSoD-IM0009)');
			$('#topmenu').fadeOut('slow', function() {
			});		
		}
	});

});
