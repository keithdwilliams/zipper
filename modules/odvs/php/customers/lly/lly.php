<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Start Pre-Config');
endif;
?>
<link rel="stylesheet" href="/SSoD-CSS0008 type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS1000"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" name="page">
			<div id="header" name="header">
				<a name="home" id="home" href="javascript:void(0);" tabindex="9">Home</a>
				<a name="logout" id="logout" href="javascript:void(0);" tabindex="10">Logout</a>
				<h2>ZIPPER</h2>
			</div>
		<div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>
			<div id="contentbox" name="contentbox">
				<h3>Lilly</h3>
				<form>
					<div>
						<?php
						if(isset($_SESSION['zipper']['hostmachine'] -> machine) && $_SESSION['zipper']['hostmachine'] -> machine != '') :
							echo '<input id="host" name="host" class="hostid config" placeholder="Fully qualified host name" type="text" style="width:405px;margin-bottom:2px;" tabindex="1" autofocus="autofocus" require="required" value="'.$_SESSION['zipper']['hostmachine'] -> machine.'" autocomplete="off" />';
						else:
							echo '<input id="host" name="host" class="hostid config" placeholder="Fully qualified host name" type="text" style="width:405px;margin-bottom:2px;" tabindex="1" autofocus="autofocus" require="required" autocomplete="off" />';
						endif;
						?>
					</div>
					<div style="float:right;">
						<select id="env" name="env" class="config" style="display:inline;height:35px;width:200px;" tabindex="3" require="required">
							<option value="">Select Environment</option>
							<option value="DEV">DEV</option>
							<option value="QA">QA</option>
							<option value="PRD">PRD</option>
						</select>
					</div>
					<div style="clear:both;"></div>
					<div>
						<button id="touch" name="touch" class="action bluebtn" tabindex="1" /><span class="label">Touch Feed File</span></button>
					</div>
					<div>
						<button id="remove" name="remove" class="action redbtn" tabindex="2" /><span class="label">Remove Feed File</span></button>
					</div>
				</form>
			</div>

		</div>
		<div id="footer">
		    <span>(C) Zipper</span>
		</div>

	</div>
</div>
</body>
</html>
