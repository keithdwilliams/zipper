<?php
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
endif;
//Assign host
$host = $_SESSION['zipper']['hostmachine'] -> machine;
$user = $_SESSION['zipper']['zipper'] -> username;
$pass = $_SESSION['zipper']['zipper'] -> password;
if(@filter_has_var(INPUT_POST, 'env') === false) :
	echo '05';
	while(@ob_end_flush());
	exit;
else:
	$ENV = @filter_input(INPUT_POST, 'env', FILTER_SANITIZE_STRING);
endif;

//Create the connection to the host
$connection = ssh2_connect($host, 22);
if(!$connection):
	echo '03';
	while(@ob_end_flush());
	exit;
endif;

//Authenticate over SSH using a plain password
if(!ssh2_auth_password($connection, $user, $pass)):
	echo '06';
	while(@ob_end_flush());
	exit;
endif;


//Run script
$cmd = 'cd /lly/warehouse/odvse_appfiles_'.$ENV.'/admin/batchthreads;touch batchoff.txt;ls';
$stream = ssh2_exec($connection, $cmd , true);
stream_set_blocking($stream, true);
$output = stream_get_contents($stream);
fclose($stream);

//Close the connection
ssh2_exec($connection, 'exit');
unset($connection);

//Analyze the results and return
if(trim($output) == 'true'):
	echo '01';
	while(@ob_end_flush());
	exit;
else:
	echo '02';
	while(@ob_end_flush());
	exit;
endif;
?>