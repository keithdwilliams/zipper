$(document).ready(function () {
	$('a#customers').bind('click', function(e){
		window.top.location.href = '/SSoD-1100';
	});
	$('a#lly').bind('click', function(e){
		window.top.location.href = '/SSoD-1101';
	});
	$('button#touch').bind('click', function(e){

		/*////////////////////////////////////////////
		/////lets validate data on the login form/////
		////////////////////////////////////////////*/

		var reason = '';
		var notice = '';
		var alert = '';
		reason += validateHost(this.form.host);
		reason += validateEnv(this.form.env);

		/*//////////////////////////////////////////////////
		/////if there is a reason the form is not valid/////
		/////then we will send a popup with the reason /////
		//////////////////////////////////////////////////*/

		if (reason != "") {
			var x = document.getElementById('formAlert');
			x.innerHTML = reason;
			$('#linkAlert').click();

			/*////////////////////////////////////////////////
			/////The data in the form appears to be valid/////
			////////////////////////////////////////////////*/

			} else {

			/*////////////////////////////////////////////////////////////
			/////Lets send the form date to the login processing page/////
			/////////////////////////////////////////////////////////////*/

			$.ajax({
				type: 'POST',
				url: '/SSoD-1102',
				data: {
					env : $('input#env').val(),
				},
				dataType: 'text',
				timeout: 10000,
				beforeSend: function() {

					/*//////////////////////////////////
					/////Preventing a double submit/////
					//////////////////////////////////*/

					$('button#touch').prop('disabled', 'true');
					$('button#remove').prop('disabled', 'true');
					$('input#env').prop('disabled', 'true');
					$('input#host').prop('disabled', 'true');

					$('div#pptxt').html('Adding Flag to Host ...<br />Please wait.');
					$('#prepage').show();
				},
				success: function(text){

					/*//////////////////////////////////////////////
					/////Create a minimum delay for the spinner/////
					//////////////////////////////////////////////*/

					var ms = 1000;
					ms += new Date().getTime();
					while (new Date() < ms){};

					/*//////////////////////////////////////////////////
					/////trim the whitespace from the text response/////
					//////////////////////////////////////////////////*/

					text = $.trim(text);

					/*////////////////////////////////////////////////////////////////
					/////Evaluate the text returned from the server ajax function/////
					////////////////////////////////////////////////////////////////*/

					switch(text) {
						case '01':
							notice = 'Successfully set flag.';
							break;
						case '02':
							alert = 'Login Failure';
							break;
						case '03':
							alert = 'Session Timeout';
							window.top.location.href = '/SSoD-0000';
							break;
						default:
							alert = text;
							break;
					}
				},
				error: function(xhr, status, error){
					var err = eval("(" + xhr.responseText + ")");
					alert = err.Message;
				},
				complete: function() {

					/*///////////////////////////////////////////////
					/////enable the inputs since we are finished/////
					///////////////////////////////////////////////*/

					$('button#remove').prop('disabled', false);
					$('button#touch').prop('disabled', false);
					$('input#host').prop('disabled', false);
					$('input#env').prop('disabled', false);

					/*//////////////////////////////////////////////////
					/////hide the spinner and load the default text/////
					//////////////////////////////////////////////////*/

					$('#prepage').hide();
					$('div#pptxt').html('');

					/*/////////////////////////////////////////////////////////
					/////load the alert box with our status for the member/////
					/////////////////////////////////////////////////////////*/

					$('div#tmpnotice').remove();
					if(notice != '') {
						$('div#alertnotice').append('<div id="tmpnotice" class="noticebox"><sub style="color:green;text-align:left;">'+notice+'</sub></div>');
					}
					if(alert != '') {
						$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">'+alert+'</sub></div>');
					}
				}
			});
		}

		/*///////////////////////////////////////////
		/////stop the default action of the form/////
		///////////////////////////////////////////*/

		e.preventDefault();
	});
	$('button#remove').bind('click', function(e){
		$.ajax({
			type: 'POST',
			url: '/SSoD-1102',
			dataType: 'text',
			timeout: 10000,
			beforeSend: function() {

				/*//////////////////////////////////
				/////Preventing a double submit/////
				//////////////////////////////////*/

				$('button#touch').prop('disabled', 'true');
				$('button#remove').prop('disabled', 'true');
				$('input#env').prop('disabled', 'true');
				$('input#host').prop('disabled', 'true');

				$('div#pptxt').html('Removing Flag from Host ...<br />Please wait.');
				$('#prepage').show();
			},
			success: function(text){

				/*//////////////////////////////////////////////
				/////Create a minimum delay for the spinner/////
				//////////////////////////////////////////////*/

				var ms = 1000;
				ms += new Date().getTime();
				while (new Date() < ms){};

				/*//////////////////////////////////////////////////
				/////trim the whitespace from the text response/////
				//////////////////////////////////////////////////*/

				text = $.trim(text);

				/*////////////////////////////////////////////////////////////////
				/////Evaluate the text returned from the server ajax function/////
				////////////////////////////////////////////////////////////////*/

				switch(text) {
					case '01':
						notice = 'Successfully removed flag.';
						break;
					case '02':
						alert = 'Login Failure';
						break;
					case '03':
						alert = 'Session Timeout';
						window.top.location.href = '/SSoD-0000';
						break;
					default:
						alert = text;
						break;
				}
			},
			error: function(xhr, status, error){
				var err = eval("(" + xhr.responseText + ")");
				alert = err.Message;
			},
			complete: function() {

				/*///////////////////////////////////////////////
				/////enable the inputs since we are finished/////
				///////////////////////////////////////////////*/

				$('button#remove').prop('disabled', false);
				$('button#touch').prop('disabled', false);
				$('input#host').prop('disabled', false);
				$('input#env').prop('disabled', false);

				/*//////////////////////////////////////////////////
				/////hide the spinner and load the default text/////
				//////////////////////////////////////////////////*/

				$('#prepage').hide();
				$('div#pptxt').html('');

				/*/////////////////////////////////////////////////////////
				/////load the alert box with our status for the member/////
				/////////////////////////////////////////////////////////*/

				$('div#tmpnotice').remove();
				if(notice != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="noticebox"><sub style="color:green;text-align:left;">'+notice+'</sub></div>');
				}
				if(alert != '') {
					$('div#alertnotice').append('<div id="tmpnotice" class="errorbox"><sub style="color:maroon;text-align:left;">'+alert+'</sub></div>');
				}
			}
		});

		/*///////////////////////////////////////////
		/////stop the default action of the form/////
		///////////////////////////////////////////*/

		e.preventDefault();
	});
});