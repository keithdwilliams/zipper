﻿<?php
require($_SERVER['DOCUMENT_ROOT'].'/api/Net/SSH2.php');
if(@filter_has_var(INPUT_POST, 'host') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:
	$host = @filter_input(INPUT_POST, 'host', FILTER_SANITIZE_STRING);
endif;
if(@filter_has_var(INPUT_POST, 'projid') === false) :
	echo '02';
	while(@ob_end_flush());
	exit;
else:

	$projid = @filter_input(INPUT_POST, 'projid', FILTER_SANITIZE_STRING);
	$projid = strtolower($projid);
endif;
$cidowner = $projid.'owner';
$user = $_SESSION['zipper']['zipper'] -> username;
$pass = $_SESSION['zipper']['zipper'] -> password;
$cmd = 'id '.$cidowner;
$ssh = @new Net_SSH2($host);
if (!$ssh->login($user, $pass)) :
	echo '02';
	exit;
else:
	$output = $ssh->exec($cmd);
	$ssh -> disconnect();
	if(strpos($output, 'No such user') == TRUE):
		echo '03';
		exit;
	endif;
	if(strpos($output, $cidowner) == TRUE):
		echo '01';
		exit;
	else:
		echo '03';
		exit;
	endif;
endif;
?>