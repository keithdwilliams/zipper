/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    $('#btnRight').click(function(e) {
        var selectedOpts = $('#hostlist option:selected');
        if (selectedOpts.length === 0) {
            alert("Nothing to move.");
            e.preventDefault();
        }
        $('#selected').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });
    $('#btnLeft').click(function(e) {
        var selectedOpts = $('#selected option:selected');
        if (selectedOpts.length === 0) {
            alert("Nothing to move.");
            e.preventDefault();
        }

        $('#hostlist').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });
    $('button#start').bind('click', function(e){   
        e.preventDefault();
        myhtml='<div id="results" style="display:block; width:400px;border:1px dashed #CCCCCC;"><h3>Results:</h3><div id="dynresults"></div></div>';
	$('$div#results').remove();
        $('div#dyncontent').append(myhtml); 

        if( $("select#selected > option").length > 0){
            //build an array of selected values
            var selectedmachines = [];
            $("select#selected > option").each(function(i, selected) {
                selectedmachines[i] = $(selected).val();
            });            
        }
        if($('input[name=mode]:checked').val()=== 'detect') {
            var mode = 'detect';
        } else if($('input[name=mode]:checked').val()=== 'harden') {
            var mode = 'harden';
        } else {
            exit();
        }
        $.ajax({
            type: 'POST',
            url: '/SSoD-1201',
            data: {
                selectedmachines:JSON.stringify(selectedmachines),
                mode : mode
            },
            dataType: 'text',
            timeout: 10000000,
            beforeSend: function() {

                /*//////////////////////////////////
                /////Preventing a double submit/////
                //////////////////////////////////*/

                $('button#start').prop('disabled', 'true');
                $('input#mode').prop('disabled', 'true');
                $('div#pptxt').html('Processing ...<br />Please wait.');
                $('#prepage').show();
            },
            success: function(text){

                $('div#dynresults').append($.trim(text)+'<br />');        
            },
            error: function(xhr,textStatus,err){
                console.log("readyState: " + xhr.readyState);
                console.log("responseText: "+ xhr.responseText);
                console.log("status: " + xhr.status);
                console.log("text status: " + textStatus);
                console.log("error: " + err);
            },
            complete: function() {

                /*///////////////////////////////////////////////
                /////enable the inputs since we are finished/////
                ///////////////////////////////////////////////*/

                $('button#start').prop('disabled', 'false');
                $('input#mode').prop('disabled', 'false');

                /*//////////////////////////////////////////////////
                /////hide the spinner and load the default text/////
                //////////////////////////////////////////////////*/

                $('#prepage').hide();
                $('div#pptxt').html('');
            }                    
        });
    });
    $('input[name=class]:radio').click(function(){
        if($('input[name=class]:checked').val() === 'ODVS'){
            var myClass = 'ODVS';
        } else if($('input[name=class]:checked').val() === 'NV') {
            var myClass = 'NV';
        } else if($('input[name=class]:checked').val() === 'ALL') {
            var myClass = 'ALL';
        }
        clearListBox('hostlist');
        $.ajax({
            type: 'POST',
            url: '/SSoD-1202',
            data: { 
                mode : 'class',
                class: myClass
            },
            dataType: 'JSON',
            timeout: 100000,
            beforeSend: function() {

                /*//////////////////////////////////
                /////Preventing a double submit/////
                //////////////////////////////////*/

                $('div#pptxt').html('Updating Available Hosts ...<br />Please wait.');
                $('#prepage').show();
            },
            success: function(text){
                var listBoxData = $.parseJSON(text);
                var hostnum = 0;
                while(hostnum < listBoxData.length) {
                    AddItemListBox(listBoxData[hostnum],listBoxData[hostnum]);
                    hostnum++;
                }       
            },
            error: function(xhr,textStatus,err){
                console.log("readyState: " + xhr.readyState);
                console.log("responseText: "+ xhr.responseText);
                console.log("status: " + xhr.status);
                console.log("text status: " + textStatus);
                console.log("error: " + err);
            },
            complete: function() {

                /*//////////////////////////////////////////////////
                /////hide the spinner and load the default text/////
                //////////////////////////////////////////////////*/

                $('#prepage').hide();
                $('div#pptxt').html('');
            }                    
        });        
    });
    $('input[name=location]:radio').click(function(){
        if($('input[name=location]:checked').val() === 'IC'){
            var myLocation = 'IC';
        } else if($('input[name=location]:checked').val() === 'OC') {
            var myLocation = 'OC';
        } else if($('input[name=location]:checked').val() === 'ALL') {
            var myLocation = 'ALL';
        }
        clearListBox('hostlist');
        $.ajax({
            type: 'POST',
            url: '/SSoD-1202',
            data: {
                mode : 'location',
                location : myLocation
            },
            dataType: 'text',
            timeout: 100000,
            beforeSend: function() {

                /*//////////////////////////////////
                /////Preventing a double submit/////
                //////////////////////////////////*/

                $('div#pptxt').html('Updating Available Hosts ...<br />Please wait.');
                $('#prepage').show();
            },
            success: function(text){

                var listBoxData = $.parseJSON(text);
                var hostnum = 0;
                while(hostnum < listBoxData.length) {
                    AddItemListBox(listBoxData[hostnum],listBoxData[hostnum]);
                    hostnum++;
                }              
            },
            error: function(xhr,textStatus,err){
                console.log("readyState: " + xhr.readyState);
                console.log("responseText: "+ xhr.responseText);
                console.log("status: " + xhr.status);
                console.log("text status: " + textStatus);
                console.log("error: " + err);
            },
            complete: function() {

                /*//////////////////////////////////////////////////
                /////hide the spinner and load the default text/////
                //////////////////////////////////////////////////*/

                $('#prepage').hide();
                $('div#pptxt').html('');
            }                    
        });        
    });
    $('input[name=type]:radio').click(function(){
        if($('input[name=type]:checked').val() === 'MT'){
            var myType = 'MT';
        } else if($('input[name=type]:checked').val() === 'MOM') {
            var myType = 'MOM';
        } else if($('input[name=type]:checked').val() === 'ALL') {
            var myType = 'ALL';
        }
        clearListBox('hostlist');
        $.ajax({
            type: 'POST',
            url: '/SSoD-1202',
            data: {
                mode : 'type',
                type : myType
            },
            dataType: 'text',
            timeout: 100000,
            beforeSend: function() {

                /*//////////////////////////////////
                /////Preventing a double submit/////
                //////////////////////////////////*/

                $('div#pptxt').html('Updating Available Hosts ...<br />Please wait.');
                $('#prepage').show();
            },
            success: function(text){

                var listBoxData = $.parseJSON(text);
                var hostnum = 0;
                while(hostnum < listBoxData.length) {
                    AddItemListBox(listBoxData[hostnum],listBoxData[hostnum]);
                    hostnum++;
                }               
            },
            error: function(xhr,textStatus,err){
                console.log("readyState: " + xhr.readyState);
                console.log("responseText: "+ xhr.responseText);
                console.log("status: " + xhr.status);
                console.log("text status: " + textStatus);
                console.log("error: " + err);
            },
            complete: function() {

                /*//////////////////////////////////////////////////
                /////hide the spinner and load the default text/////
                //////////////////////////////////////////////////*/

                $('#prepage').hide();
                $('div#pptxt').html('');
            }                    
        });        
    });
});

function clearListBox(listboxID) {
    // returns 1 if all items are sucessfully removed
    // otherwise returns zero.
    var mylistbox = document.getElementById(listboxID);
    if(mylistbox == null)
        return 1;
    while(mylistbox.length > 0) {
        mylistbox.remove(0);
    }
    return 1;
}

function AddItemListBox(Text,Value) {
    // Create an Option object
    var opt = document.createElement("option");
    // Add an Option object to Drop Down/List Box
    document.getElementById("DropDownList").options.add(opt);
    // Assign text and value to Option object
    opt.text = Text;
    opt.value = Value;
}


