<?php
ini_set("memory_limit","1000M");
set_time_limit(0);
//Assign connection variables
$user = $_SESSION['zipper']['zipper'] -> username;
$pass = $_SESSION['zipper']['zipper'] -> password;

$machines = filter_var_array(json_decode($_POST['selectedmachines']));
if($_POST['mode']==='detect'):
    foreach ($machines as $machine) {
        $host = strtolower($machine.'.vsp.sas.com');
        echo trim(rundetect ($machine, $host, $user, $pass, $cmd));
    }
    exit;
elseif($_POST['mode']==='harden'):
    foreach ($machines as $machine) {
        $host = strtolower($machine.'.vsp.sas.com');
        echo trim(runharden ($machine, $host, $user, $pass, $cmd));
    }
    exit;    
endif;
function rundetect ($machine, $host, $user, $pass, $cmd) {	
    $connection = ssh2_connect($host, 22);
    $clean = 'rm -f /tmp/findweakjboss.sh';
    $cmd = 'echo '.$pass.' | sudo -S /tmp/findweakjboss.sh';
    if(!$connection):
        $updatetext = 'Unable to connect.';
        dbupdate($updatetext, $machine);
        return $host.'<br />'.$updatetext.'<br />';
    endif;	
    //Authenticate over SSH using a plain password
    if(!ssh2_auth_password($connection, $user, $pass)):
        $updatetext = 'Authentication failure.';
        dbupdate($updatetext, $machine);
        return $host.'<br />'.$updatetext.'<br />';
    endif;
    //Run script
    ssh2_exec($connection, $clean, true);
    $ssh2_scp_send = ssh2_scp_send($connection, "/var/www/html/scripts/findweakjboss.sh", '/tmp/findweakjboss.sh', 0555);
    if(!$ssh2_scp_send):
        ssh2_exec($connection, $clean, true);
        $updatetext = 'Upload to host failed.';
        dbupdate($updatetext, $machine);
        return $host.'<br />'.$updatetext.'<br />';
    endif;
    $stream = ssh2_exec($connection, $cmd, true);
    if(!$stream):
        ssh2_exec($connection, $clean, true);
        $updatetext = 'No results were returned.';
        dbupdate($updatetext, $machine);
        return $host.'<br />'.$updatetext.'<br />';
    endif;
    stream_set_blocking($stream, true);
    $output = stream_get_contents($stream);
    $output = str_ireplace("[sudo] password for ".$user.":", "", $output);
    $remove = "We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.";
$output = str_ireplace($remove,'',$output);
    fclose($stream);
    ssh2_exec($connection, $clean, true);
    //Close the connection
    ssh2_exec($connection, 'exit');
    //look at the output and update the database accordingly to record the weak, hardened server 
    dbupdate($output, $machine);
    return $host.'<pre>'.trim($output).'</pre>';    
}
function runharden ($machine, $host, $user, $pass, $cmd) {	
    $connection = ssh2_connect($host, 22);
    $clean = 'rm -f /tmp/jbhard.sh /tmp/dfindname.txt';
    $cmd = 'echo '.$pass.' | sudo -S /tmp/jbhard.sh';
    if(!$connection):
        $updatetext = 'Unable to connect.';
        //dbupdate($updatetext, $machine);
        return $host.'<br />'.$updatetext.'<br />';
    endif;	
    //Authenticate over SSH using a plain password
    if(!ssh2_auth_password($connection, $user, $pass)):
        $updatetext = 'Authentication failure.';
        //dbupdate($updatetext, $machine);
        return $host.'<br />'.$updatetext.'<br />';
    endif;
    //Run script
    ssh2_exec($connection, $clean, true);
    $ssh2_scp_send = ssh2_scp_send($connection, "/var/www/html/scripts/jbhard.sh", '/tmp/jbhard.sh', 0555);
    if(!$ssh2_scp_send):
        ssh2_exec($connection, $clean, true);
        $updatetext = 'Upload to host failed.';
        //dbupdate($updatetext, $machine);
        return $host.'<br />'.$updatetext.'<br />';
    endif;
    $stream = ssh2_exec($connection, $cmd, true);
    if(!$stream):
        ssh2_exec($connection, $clean, true);
        $updatetext = 'No results were returned.';
        //dbupdate($updatetext, $machine);
        return $host.'<br />'.$updatetext.'<br />';
    endif;
    stream_set_blocking($stream, true);
    $output = stream_get_contents($stream);
    $output = str_ireplace("[sudo] password for ".$user.":", "", $output);
    $remove = "We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.";
$output = str_ireplace($remove,'',$output);
    fclose($stream);
    ssh2_exec($connection, $clean, true);
    //Close the connection
    ssh2_exec($connection, 'exit');
    //look at the output and update the database accordingly to record the weak, hardened server 
    //dbupdate($output, $machine);
    return $host.'<pre>'.trim($output).'</pre>';    
}
function dbupdate($updatetext, $machine){
    $mysqli = new mysqli(HOSTi, DBUSERi, PASSi, DBi);
    if(mysqli_connect_errno()):
        $mysqli -> close(); 
        while(@ob_end_flush());
        exit;
    else :
        if($stmt = $mysqli->prepare("UPDATE zipper_vsphosts SET jbresult = ? WHERE host = ? LIMIT 1;")):
            $stmt->bind_param('ss', $updatetext, $machine);
            $stmt->execute();
            $mysqli -> close();
        else:
            $mysqli -> close();
            exit;
        endif;
    endif;    
}