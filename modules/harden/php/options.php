﻿<!-- Verify user and load header -->
<?php
set_time_limit(0);
if(!isset($_SESSION['zipper']['zipper'] -> loggedin) || $_SESSION['zipper']['zipper'] -> loggedin != '1') :
	header("location:/SSoD-0000");
else:
	echo $_SESSION['zipper']['zipper'] -> htmlhead('Zipper | Harden JBoss');
endif;
?>
<!-- Custom CSS and Javascript -->
<link rel="stylesheet" href="/SSoD-CSS0005" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="/SSoD-JS1200"></script>
<?php echo $_SESSION['zipper']['zipper'] -> prepage(); ?>
	<div id="page" name="page">
            <div id="header" name="header">
                <a name="home" id="home" href="javascript:void(0);"tabindex="9">Home</a>
		<a name="logout" id="logout" href="javascript:void(0);" tabindex="10">Logout</a>
		<h2>ZIPPER</h2>
            </div>
            <div id="content">
		<?php echo $_SESSION['zipper']['zipper'] -> popalert(); ?>	
                    <div style="width:475px;border-style:solid;padding: 4px 4px 20px 4px;border-width:1px;border-color:blue;margin:0px auto;">
			<h3 id="jbtitle" id="name">JBoss Hardening</h3>
			<form style="width:465px;margin: 0px auto;">
                            <div style="border: 1px gray solid;padding: 3px;text-align: left;">
                                <h3>Action</h3>
                                <div>
                                    <h4 style="margin-top: .3em;margin-bottom: .3em;display:inline-block;width:80px;text-align:right;">Mode</h4>
                                    <div style="width:120px;text-align:left;display:inline-block;">
                                        <input type="radio" name="mode" value="detect" checked="" />Detect
                                    </div>
                                    <div style="width:120px;text-align:left;display:inline-block;">
                                        <input type="radio" name="mode" value="harden" />Harden
                                    </div>
                                </div>
                                <!--
                                <hr />
                                <h3>Host List</h3>
                                <div>
                                    <h4 style="margin-top: .3em;margin-bottom: .3em;display:inline-block;width:80px;text-align:right;">Host Class</h4>
                                    <div style="width:120px;text-align:left;display:inline-block;">
                                        <input type="radio" name="class" value="ODVS" />ODVS
                                    </div>
                                    <div style="width:120px;text-align:left;display:inline-block;">                                    
                                        <input type="radio" name="class" value="NV" />Non-Validate
                                    </div>
                                    <div style="width:120px;text-align:left;display:inline-block;">                                    
                                        <input type="radio" name="class" value="ALL" checked="" />All
                                    </div>
                                </div>
                                <div>
                                    <h4 style="margin-top: .3em;margin-bottom: .3em;display:inline-block;width:80px;text-align:right;">Location</h4>
                                    <div style="width:120px;text-align:left;display:inline-block;">
                                        <input type="radio" name="location" value="IC" />Cary
                                    </div>
                                    <div style="width:120px;text-align:left;display:inline-block;">                                    
                                        <input type="radio" name="location" value="OC" />Outside Cary
                                    </div>
                                    <div style="width:120px;text-align:left;display:inline-block;">                                    
                                        <input type="radio" name="location" value="ALL" checked="" />All
                                    </div>
                                </div>
                                <div>
                                    <h4 style="margin-top: .3em;margin-bottom: .3em;display:inline-block;width:80px;text-align:right;">Type</h4>
                                    <div style="width:120px;text-align:left;display:inline-block;">
                                        <input type="radio" name="type" value="MT" />Mid-Tier
                                    </div>
                                    <div style="width:120px;text-align:left;display:inline-block;">                                      
                                        <input type="radio" name="type" value="MOM" />MOM
                                    </div>
                                    <div style="width:120px;text-align:left;display:inline-block;">                                      
                                        <input type="radio" name="type" value="ALL" checked="" />All
                                    </div>
                                </div> 
                                -->
                            </div>
                            <br />
                            <div id="dyncontent">
                                <div style="width:390px;">
                                    <div style="width:160px;float:left;">
                                        <b>Available Hosts:</b><br/>
                                        <?php 
                                        $mysqli = new mysqli(HOSTi, DBUSERi, PASSi, DBi);
                                        if(mysqli_connect_errno()):
                                            $mysqli -> close(); 
                                            while(@ob_end_flush());
                                            exit;
                                        else :
                                            if($stmt = $mysqli->prepare("SELECT host FROM zipper_vsphosts WHERE role='midtier';")):
                                                $stmt->execute();
                                                $stmt->store_result();
                                                $stmt->bind_result($host);
                                            else:
                                                exit;
                                            endif;
                                        endif;
                                        echo '<select multiple="multiple" id="hostlist" style="width:160px;height:200px;">';
                                        while($stmt->fetch()){
                                            echo  '<option value="'.trim($host).'">'.trim($host).'</option>';
                                        }
                                        echo '</select>';
                                        ?>
                                    </div>
                                    <div style="float:left;width:50px;text-align:center;vertical-align:middle;padding-left:10px;padding-right:10px;padding-top:5px;">
                                        <input type='button' id='btnRight' value ='  >  '/>
                                        <br /><input type='button' id='btnLeft' value ='  <  '/>
                                    </div>
                                    <div style='width:160px;float:right'>
                                        <b>Selected: </b><br/>
                                        <select multiple="multiple" id='selected' style="width:160px;height:200px;">  
                                        </select>
                                    </div>
                                    <div style="clear:both;">
                                    </div>
                                </div>
				<div id="buttonholder">
                                    <button id="start" name="start" class="action bluebtn" tabindex="3"><span class="label">Start</span></button>
				</div>
                            </div>
			</form>
                    </div>
		</div>
		<div id="footer">
		    <span style="float:right;">(C) Zipper</span>
		</div>
	</div>
</div>
</body>
</html>

