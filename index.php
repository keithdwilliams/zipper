<?php
if(Browser::is_old_browser()):
	echo'
	<style>
	.app_old_browser_notice {
		background-color:#ff0000;
		color: white;
		margin:20px;
		padding:10px;
	}

	.app_old_browser_notice a {
		color: white;
	}
	</style>
	<div class="app_old_browser_notice">
		<h2>Old Browser</h2>
		<h3>Please consider upgrading your browser. Doing so will improve experience with the site.</h3>
		<hr />
		<p>Please click one of them below.</p>
		<ul>
			<li><a href="http://www.mozilla.org/en-US/firefox/fx/" target="_blank">Mozilla Firefox</a></li>
			<li><a href="https://www.google.com/chrome" target="_blank">Google Chrome</a></li>
			<li><a href="http://www.apple.com/safari/" target="_blank">Safari</a></li>
			<li><a href="http://www.opera.com/download/" target="_blank">Opera</a></li>
			<li><a href="http://www.microsoft.com/windows/ie/" target="_blank">Internet Explorer (Windows)</a></li>
		</ul>
	</div>';
else :
	header("location:/SSoD-0000");
endif;
?>
