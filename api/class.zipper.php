<?php
/**
 * Zipper Class
 *
 * Collection of commonly used functions
 *
 * @package		Zipper
 * @subpackage		Libraries
 * @author		Keith Williams <keith.williams@sas.com>
 * @link		http://zipper.na.sas.com
 * @since		Version 1.0
 * @license		LGPL
 */

class Zipper {
	public $username;
	public $password;
	public $loggedin;
	public $displayname;
	public $role;
	public function logger($content) {
		$filename = $_SERVER['DOCUMENT_ROOT'].'/log/message.txt';
		echo $content;
		exit;
		if (is_writable($filename)) {
			if (!$handle = fopen($filename, 'a')) {
				exit;
			}
			if (fwrite($handle, $content) === FALSE) {
				echo $content;
				exit;
			}
			fclose($handle);
		}
	}
	public function htmlhead($title = 'Zipper') {
		$head = '';
		$head .= '<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="utf-8" />
			<title>'.$title.'</title>
			<link rel="stylesheet" href="/SSoD-CSS0000" type="text/css" media="screen" />
			<link rel="stylesheet" href="/SSoD-CSS0001" type="text/css" media="screen" />
			<link rel="stylesheet" href="/SSoD-CSS0006" type="text/css" media="screen" />
			<script type="text/javascript" language="javascript" src="/SSoD-JS0000"></script>
			<script type="text/javascript" language="javascript" src="/SSoD-JS0001"></script>
			<script type="text/javascript" language="javascript" src="/SSoD-JS0002"></script>
			<script type="text/javascript" language="javascript" src="/SSoD-JS0003"></script>
			<script type="text/javascript" language="javascript" src="/SSoD-JS0004"></script>
			<script type="text/javascript" language="javascript" src="/SSoD-JS0005"></script>
			<script type="text/javascript" language="javascript" src="/SSoD-JS0006"></script>
			<script type="text/javascript" language="javascript" src="/SSoD-JS0007"></script>
			<script type="text/javascript" language="javascript" src="/SSoD-JS0008"></script>
			<script type="text/javascript" language="javascript" src="/SSoD-JS0009"></script>';
			if(isset($_SESSION['alert']) && !empty($_SESSION['alert'])) {
				$head .= '<script type="text/javascript">
					$(document).ready(function(){
						var tog = document.getElementsByName("divalert");
						for (var t = 0; t < tog.length; t++) {
							tog[t].style.display = "block";
						}
						var box = document.getElementsByName("alertbox");
						for (var i = 0; i < box.length; i++) {
							box[i].innerHTML = "'.$_SESSION["alert"].'";';
							$_SESSION['alert'] = '';
						$head .= '}
					});
				</script>';
			} else {
				$head .= '<script type="text/javascript">
					$(document).ready(function(){
						var tog = document.getElementsByName(divalert);
						for (var t = 0; t < tog.length; t++) {
							tog[t].style.display = "none";
						}
					});
				</script>';
			}
			if(isset($_SESSION['notice']) && !empty($_SESSION['notice'])) {
				$head .= '<script type="text/javascript">
					$(document).ready(function(){
						var tog = document.getElementsByName("divnotice");
						for (var t = 0; t < tog.length; t++) {
							tog[t].style.display = "block";
						}
						var box = document.getElementsByName("noticebox");
						for (var i = 0; i < box.length; i++) {
							box[i].innerHTML = "'.$_SESSION['notice'].'";';
							$_SESSION['notice'] = '';
						$head .= '}
					});
				</script>';
			} else {
				$head .= '<script type="text/javascript">
					$(document).ready(function(){
						var tog = document.getElementsByName("divnotice");
						for (var t = 0; t < tog.length; t++) {
							tog[t].style.display = "none";
						}
					});
				</script>';
			}
		return $head;
	}
	public function prepage($message = 'Zipper is loading') {
		$prepage = '';
		$prepage .= '<script type="text/javascript">


			var a, pageTimer;  //declare var
			function startClock() {
				pageTimer = 0;  //start at 0
				a = window.setInterval("tick()",60000);  //run func tick every minute....  (60 sec x 1000 ms = 1min)
			}
			function tick() {
				pageTimer++;  //increment timer
				if (pageTimer == 240) {warnuser()};  //if 28 min without activity
			}
			function warnuser() {
				if (confirm("There has been no activity for some time.\nClick OK if you wish to continue your session,\nor click Cancel to log out.\nFor your security if you are unable to respond to this message\nwithin two minutes you will be logged out automatically.")) {
					location.reload();
				} else {
					document.location.href="/SSoD-0003";
				}
			}
		</script>
		</head>
		<body onLoad="waitPreloadPage();startClock();">';
			if($_SERVER['REDIRECT_URL'] != '/SSoD-0000'):
				$prepage .= '<div id="topdiv" name="topdiv">
				'.$_SESSION['zipper']['zipper'] -> displayname.'
				</div>
				<div id="topmenu" name="topmenu" val="closed">
				</div>';
			endif;

		$prepage .= '<div id="prepage" name="prepage">
			<div id="pptxt" name="pptxt" style="font-weight:bold;">'.$message.'</div>
			<div><img src="/SSoD-IM0014" class="prepage"></div>
		</div>
		<div id="wrapper">';
		return $prepage;
	}
	public function popalert() {
		$popalert = '';
		$popalert .= '<!-- Popup box -->
		<div id="alert" class="popup_block">
			<h3 style="color:#dd3c10;"></h3>
			<p id="formAlert" name="formAlert"></p>
			<div class="buttons" style="width:40px;margin:0px auto;padding-bottom:5px;">
				<button class="action greenbtn" id="closeWindow">
					<span class="label">Close</span>
				</button>
			</div>
		</div>
		<a id="linkAlert" name="linkAlert" href="#?w=400" rel="alert" class="poplight"></a>
		<!-- Alert/Notice Box -->
		<div name="alertnotice" id="alertnotice" style="width:370px;margin:0px auto;padding-bottom:10px;">
			<div style = "padding-top:1px;padding-bottom:2px;">
				<div class="errorbox" name="divalert" id="divalert">
					<sub name="alertbox" id="alertbox" style="color:maroon;text-align:left;"></sub>
				</div>
			</div>
			<div style = "padding-top:2px;padding-bottom:1px;">
				<div class="noticebox" name="divnotice" id="divnotice">
					<sub name="noticebox" id="noticebox" style="color:green;text-align:left;"></sub>
				</div>
			</div>
		</div>';
		return $popalert;
	}

	public function runcmd ($host, $user, $pass, $cmd) {
		$connection = ssh2_connect($host, 22);
		if(!$connection):
			return '1';
		endif;
		//Authenticate over SSH using a plain password
		if(!ssh2_auth_password($connection, $user, $pass)):
			return '2';
		endif;
		//Run script
		$stream = ssh2_exec($connection, $cmd, true);
		stream_set_blocking($stream, true);
		$output = stream_get_contents($stream);
		fclose($stream);

		//Close the connection
		ssh2_exec($connection, 'exit');
		unset($connection);
		return '<pre>'.$output.'</pre>';
	}

}
