﻿<?php
/**
* Zipper Hostname Class
*
* Sets and maintains state for an individul user.
* @access		Public
* @author		Keith Williams <keith.williams@sas.com>
* @copyright 		Copyright (c) 2013, Keith Williams
* @internal 		This class is still is in beta and needs to be tested.
* @package		Zipper
* @subpackage		Libraries
* @link			https://zipper.na.sas.com
* @since		Version 1.0
* @license		LGPL
*/

class HostMachine {

	public $machine; 
	public $sasversion;
	public $projectid;
	public $ownerid;
	public $ownerpwd;
	public $runid;
	public $ip;
	public $location;
	public $mode;

	/**
	* Constructor - Setup message parameters
	*
	* The constructor needs to be called with the machine address. It returns true or false. 
	**/
	function __construct($hostid) {
		$ip = gethostbyname($hostid);
		if(filter_var($ip, FILTER_VALIDATE_IP)) {
			$this -> machine = strtolower($hostid);
			$this -> ip = $ip;
		} else {
			echo 'false';
			while(@ob_end_flush());
			exit;
		}
        }		
	/*
	*magic method to return when trying to get a property which does not exist
	*/
	
	public function __get($key) {
		echo "The property: ".$key." does not exist.";
	}

	/*
	*magic method to return when trying to set a property which does not exist
	*/
	
	public function __set($key, $value) {
		echo "Unable to set property ".$key." with ".$value." because it does not exist.";
	}
	
	public function get($key) {
		return $this -> {$key};
	}
	
	public function set($key, $value) {
		$this -> {$key} = $value;
	}
	
	public function __toString() {
		echo $this -> name;
	}
	
	public function __call($name, $args) {
		var_dump($name);
		echo "\n";
		var_dump($args);
		echo "\n";	
	}

}
?>