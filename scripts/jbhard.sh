#!/bin/sh
SASOWNER=$(hostname | sed 's/ *[0-9].*/owner/')
WHOAMI="/usr/bin/whoami"
if [ `$WHOAMI` != "$SASOWNER" ]
then
        #echo Invoking su $SASOWNER -c $0 $1
        su "$SASOWNER" -c "$0 $1"
        exit $?
fi
if [ -s /tmp/dfindname.txt ]
then
    
    rm -f /tmp/dfindname.txt
fi
find / -type f -iname jbossorg-eula.txt 2>/dev/null | grep -v 'Permission denied'>/tmp/dfindname.txt
if [ -s /tmp/dfindname.txt ]
then
    sed -i 's/mod_cluster\/JBossORG-EULA.txt/jboss-as/g' /tmp/dfindname.txt
    sed -i 's/\/JBossORG-EULA.txt//g' /tmp/dfindname.txt
    while read p <&3; do
        for i in ${p}/server/SASServer*;
            do 
                file="${i}/conf/props/console-users.properties"
                if [ -f "$file" ]
                then
                        echo "${i} is already Hardened"
                else
                    echo "Trying to harden the jboss SASServer${i}"
                    cp ${i}/conf/props/jmx-console-users.properties ${i}/conf/props/console-users.properties 2>/dev/null;
                    if [ "$?" != "0" ]; then
                        echo "$[Error] Copy to backup failed for jmx-console-users.properties."
                    else 
                        echo "Backed up & copied jmx-console-users.properties."
                    fi
                    cp ${i}/conf/props/jmx-console-roles.properties ${i}/conf/props/console-roles.properties 2>/dev/null;
                    if [ "$?" != "0" ]; then
                        echo "[Error] Copy to backup failed for jmx-console-roles.properties."
                    else 
                        echo "Backed up & copied jmx-console-roles.properties."
                    fi
                    sed -i 's/admin/sasadmin/g' ${i}/conf/props/console-roles.properties 2>/dev/null
                    if [ "$?" != "0" ]; then
                        echo "[Error] Find and replace admin with sasadmin failed!"
                    else 
                        echo "Found and replaced admin with sasadmin."
                    fi
                    sed -i 's/admin\=admin/sasadmin\=saspwd/g' ${i}/conf/props/console-users.properties 2>/dev/null
                    if [ "$?" != "0" ]; then
                        echo "[Error] Find and replace default password failed!"
                    else 
                        echo "Default password has been changed."
                    fi
                    cp ${i}/conf/login-config.xml ${i}/conf/login-config.xml.orig 2>/dev/null;
                    if [ "$?" != "0" ]; then
                        echo "[Error] Backup and copy failed for login-config.xml."
                    else 
                        echo "Backed up & copied login-config.xml."
                    fi
                    sed -i 's/jmx-console-users.properties/console-users.properties/g' ${i}/conf/login-config.xml 2>/dev/null
                    if [ "$?" != "0" ]; then
                        echo "[Error] Update jmx-console to use console-users.properties failed!"
                    else 
                        echo "Update jmx-console to use console-users.properties suceeded."
                    fi
                    sed -i 's/web-console-users.properties/console-users.properties/g' ${i}/conf/login-config.xml 2>/dev/null
                    if [ "$?" != "0" ]; then
                        echo "[Error] Update web-console to use console-users.properties failed!"
                    else 
                        echo "Update web-console to use console-users.properties suceeded"
                    fi
                    sed -i 's/jmx-console-roles.properties/console-roles.properties/g' ${i}/conf/login-config.xml 2>/dev/null
                    if [ "$?" != "0" ]; then
                        echo "[Error] Update jmx-console to use console-roles.properties failed!"
                    else 
                    echo "Update jmx-console to use console-roles.properties suceeded."
                    fi
                    sed -i 's/web-console-roles.properties/console-roles.properties/g' ${i}/conf/login-config.xml 2>/dev/null
                    if [ "$?" != "0" ]; then
                        echo "[Error] Update web-console to use console-roles.properties failed!"
                    else 
                        echo "Update web-console to use console-roles.properties suceeded."
                    fi
                    if [ -d "${i}/staging" ]
                    then
                        echo "${i}/staging directory  exists!"
                    else
                        mkdir ${i}/undeploy ${i}/staging 2>/dev/null;
                        if [ "$?" != "0" ]; then
                            echo "[Error] copy failed!"
                        else 
                            echo "Backed up & copied jmx-console-users.properties."
                        fi
                    fi
                fi
            done
    done 3< /tmp/dfindname.txt
    rm -f /tmp/dfindname.txt
else
        echo 'Jboss was not found'
fi