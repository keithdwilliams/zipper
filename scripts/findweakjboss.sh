#!/bin/sh
find / -type f -iname jbossorg-eula.txt 2>/dev/null | grep -v 'Permission denied'>dfindname.txt
if [ -s dfindname.txt ]
then
    sed -i 's/mod_cluster\/JBossORG-EULA.txt/jboss-as/g' dfindname.txt
    sed -i 's/\/JBossORG-EULA.txt//g' dfindname.txt
    while read p <&3; do
        for i in ${p}/server/SASServer*;
            do
                file="${i}/conf/props/console-users.properties"
                if [ -f "$file" ]
                then
                        echo "01-${i} is Hardened"
                else
                        echo "02-${i} is Weak."
                fi
            done
    done 3< dfindname.txt
else
    clear;
    echo 'Jboss was not found.'
fi

